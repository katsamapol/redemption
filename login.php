<?php
session_start();
require_once("connect.inc.php");
require_once("auth/hash.php");

$cookie_duration = 8 * 60 * 60; // 8 hours in seconds persistent cookie

if(isset($_GET['try']) && $_GET['try'] == 1) {
    $user = $_POST["user"];
    $pass = $_POST["pass"];
    $check = checkPassword($user, $pass, "user", $pdo);

    if($check === true) {
        $word = "";
        try {
            $getUserQuery = $pdo->prepare("SELECT * FROM `user` WHERE `username` = :username");
            $getUserQuery->bindParam(':username', $user, PDO::PARAM_STR);
            $getUserQuery->execute();
        } catch(PDOException $e) {
            die("Query failed: " . $e->getMessage());
        }

        $userResult = $getUserQuery->fetch();

        if(isset($_POST["remember"])) {
            // If "Remember Me" is checked, set a persistent cookie
            setcookie("username", $user, time() + $cookie_duration, "/");
        } else {
            // If "Remember Me" is not checked, use session
            $_SESSION["username"] = $user;
        }

        ?>
        <script type='text/javascript'>
            window.location = 'index.php';
        </script>
        <?php
        exit();
    } else {
        $word = "Invalid credentials.";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Redemption</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <link rel="stylesheet" type="text/css" href="css/standard.css">
    <link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>

<div align="center" style='color:red;'><?php echo (isset($word) ? $word : false); ?></div>

<div class="login">
    <h1>Login</h1>
    <form action="login.php?try=1" method="post">
        <input class="mlogin" type="text" name="user" placeholder="Username" required="required" /><br><br>
        <input class="mlogin" type="password" name="pass" placeholder="Password" required="required" /><br><br>
        <label><input class="cbox" type="checkbox" name="remember">Remember me</label><br><br>
        <input class="mlogin" type="submit" value="Login" name="submit">
    </form>
</div>

</body>
</html>
