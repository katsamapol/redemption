<?php
require "global.php";
$searchArea = $_POST["searchArea"];

if($searchArea != ""){
    //echo "".$searchArea."<br>";

}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Self Service</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles/standard.css">
    <style>
    /*basic style*/
    * {
        /*font-size: 150%;*/
        /*border: 1px solid red;*/
        font-size: 1.05em;
    }
    .container {
        width: 100%;
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
        justify-content: center;
        align-content: flex-start;
        text-align: center;
    }
    .titleText {
        order: 1;
        font-weight: bold;
        margin: 30px 0;
        font-size: 1.2em;
    }
    .idTypeText {
        order: 2;
        margin-bottom: 30px;
    }
    .idCardText {
        order: 3;
        margin-bottom: 30px;
    }
    .submitButton {
        order: 4;
    }

    .submitButton input[type="button"]{
        display: block;
        margin: auto;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        background-color: rgba(250,192,61,1); /* SET 137c */
        border: none;
        color: black;
        text-align: center;
        padding: 10px 20px;
        /*transition: all 0.5s;*/
        cursor: pointer;
        text-decoration: none;
    }
    input[type="text"] {
        width:  80%;
        height: 40px;
        text-align: center;
        border-radius: 10px;
        -moz-border-radius: 10px;
        -webkit-border-radius: 10px;
        -o-border-radius: 10px;
    }
    input[type="radio"] {
        display:none;
    }
    input[type="radio"] + label{
        display:inline-block;
        padding: 5px 10px;
        vertical-align:middle;
        /*background:url(check_radio_sheet.png) left top no-repeat;*/
        border: 1px solid #ccc;
        color: #ccc;
        cursor:pointer;
        font-size: 110%;
        margin: 0px 10px;
        border-radius: 10px;
        -moz-border-radius: 10px;
        -webkit-border-radius: 10px;
        -o-border-radius: 10px;
    }
    input[type="radio"]:checked + label{
        /*background:url(check_radio_sheet.png) -19px top no-repeat;*/
        border: 1px solid rgba(250,192,61,1);
        background-color: rgba(250,192,61,1); /* SET 137c */
        color: black;
    }
    .overlay {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background: rgba(0, 0, 0, 0.7);
      transition: opacity 500ms;
      visibility: hidden;
      opacity: 0;
      z-index: 99;
    }
    .overlay:target {
      visibility: visible;
      opacity: 1;
    }

    .popup {
      margin: 70px auto;
      padding: 20px;
      background: #fff;
      border-radius: 5px;
      max-width: 70%;
      position: relative;
      transition: all 5s ease-in-out;
      overflow: auto;
    }

    .popup .close {
      position: absolute;
      top: 10px;
      right: 20px;
      transition: all 200ms;
      font-size: 30px;
      font-weight: bold;
      text-decoration: none;
      color: #333;
    }
    .popup .close:hover {
      color: orange;
    }
    .popup .content {
      overflow: auto;
      text-align: center;
    }
    .imgAlert {
        width: 50px;
        vertical-align: middle;
    }
    .alert {
        font-size: 1.05em;
    }
    .green {
        color: #006400;
    }
    .blue {
        color: #000080;
    }
    .red {
        color: #8B0000;
    }
    /*639 and above*/
    @media screen and (min-width:639px) {
        .container {
            font-size: 1.5em;
        }
        .titleText {
            font-size: 1.3em;
        }
        .popup .content{
            text-align:left;
            width: 80%;
            padding: 40px;
        }
        input[type="radio"] + label{
            margin: 0px 30px;
        }
        .alert {
            font-size: 1.3em;
            display:inline;
        }
        .imgAlert {
            width: 70px;
            margin: 0 20px;
        }
    }
    </style>
    <script src="ajax_self_service.js"></script>
    <script src="js/jquery-3.1.0.min.js"></script>
</head>
<body>
    <!-- popup code -->
    <div id="popup" class="overlay">
        <div class="popup">
            <a class="close" href="#">X</a>
            <div class="content" align="center">
                <div id="displayMessage">TEST</div>
            </div>
        </div>
    </div>
    <!-- popup code -->
    <div class="container">
        <div class="titleText">จุดตรวจสอบสิทธิ์ในการรับโปรโมชั่น</div>
        <div class="idTypeText">
            <input type="radio" name="idType" value="thai" id="idCardRadio" checked><label for="idCardRadio">คนไทย</label>
            <input type="radio" name="idType" value="foreign" id="passportRadio"><label for="passportRadio">Foreigner</label>
        </div>
        <div class="idCardText">
            <input type="text" name="idCard" id="idCardInput" maxlength="13" placeholder="ระบุรหัสบัตรประชาชนที่นี่ / Your passport here" value="">
        <input type="hidden" name="hidden_id" id="hidden_id" value="">
        </div>
        <div class="submitButton">
            <input type="button" value="ตรวจสอบ" onclick="checkTest($('#idCardInput').val(),$('input[name=idType]:checked').val(),'set')"">
        </div>
    </div>
</body>
<!--ท่านได้ทำการแลกรับโปรโมชั่นภายในงานนี้เรียบร้อยแล้ว-->
<!--ท่านมีสิทธิ์รับโปรโมชั่นสำหรับลูกค้าทั่วไป-->
<!--ท่านมีสิทธิ์รับโปรโมชั่นสำหรับลูกค้าใหม่-->