<?php

header("Content-Type: text/html; charset=utf-8");

require_once("connect.inc.php");
require_once("auth.inc.php");

$what = get_session_or_cookie('what');
$project = get_session_or_cookie('project');

$id        = $_GET['o'];
$item      = $_GET['a'];
$card_type = $_GET['str3'];
$check     = 0;
$sum       = 0;
$word      = "";

if ($card_type == "idcard") {
    if (strlen($id) == 13) {
        for ($i = 0; $i < 12; $i++) {
            $sum += (($id[$i]) * (13 - $i));
        }
        if (((11 - $sum % 11) % 10) != ($id[$i])) {
            $check = 0;
        } else {
            $check = 1;
        }
    } else {
        $check = 0;
    }
} else {
    if (strlen($id) <= 4) {
        $check = 0;
    } else {
        $check = 1;
    }
}

if ($check == 1) {
    if ($what == "set") {
        $word = "";

        //check number of stock
        $sql_s = "select count(*) as `rs_s` from `set` where `id` = '" . $id . "' and `stock` = '1' ORDER BY `id_id` DESC";
        try {
            $getCountStockQuery = $pdo->query($sql_s);
        }
        catch (PDOExeption $e) {
            die("Query failed: " . $e . getMessage());
        }
        $rs_s = $getCountStockQuery->fetchColumn();
        if ($rs_s == 0) {
            $word .= "<font color=green size=5>รหัสบัตรประชาชนนี้ ไม่เคยแลกรับรางวัลใน หุ้น </font><br>";
        }

        //check number of tfex
        $sql_t = "select count(*) as `rs_t` from `set` where `id` = '" . $id . "' and `futures` = '1' ORDER BY `id_id` DESC";
        try {
            $getCountTFEXQuery = $pdo->query($sql_t);
        }
        catch (PDOExeption $e) {
            die("Query failed: " . $e . getMessage());
        }
        $rs_t = $getCountTFEXQuery->fetchColumn();
        if ($rs_t == 0) {
            $word .= "<font color=blue size=5>รหัสบัตรประชาชนนี้ ไม่เคยแลกรับรางวัลใน อนุพันธ์ </font><br>";
        }

        //check number of total
        $sql = "select count(*) as `rs` from `set` where `id` = '" . $id . "' ORDER BY `id_id` DESC";
        try {
            $getCountQuery = $pdo->query($sql);
        }
        catch (PDOExeption $e) {
            die("Query failed: " . $e . getMessage());
        }
        $rs = $getCountQuery->fetchColumn();
        if ($rs != 0) {
            $word .= "รหัสบัตรประชาชนนี้";
            $word .= "เคยแลกของแล้ว <font color=\"#FF0000\">" . $rs . "</font> ครั้ง";
            $word .= "<br>ตามชื่องานต่อไปนี้";
            $c1    = 1;
            $sql_a = "select * from `set` where id = '" . $id . "' ORDER BY id_id DESC";
            try {
                $getAllQuery = $pdo->query($sql_a);
            }
            catch (PDOExeption $e) {
                die("Query failed: " . $e . getMessage());
            }
            $allResult = $getAllQuery->fetchAll();
            foreach ($allResult as $rs2) {
                $con = 0;
                if ($rs2['project'] == $project) {
                    $word .= "<br><font color=\"#FF0000\">" . $c1 . ". งาน " . $rs2['project'] . "</font>&nbsp;";
                    $setok = 2;
                } elseif ($rs2['project'] == "TFEX IMPORT DATA") {
                    $word .= "<br><font color=\"#FF0000\">" . $c1 . ". งาน " . $rs2['project'] . "</font>&nbsp;";
                    $setok = 2;
                } else {
                    $word .= "<br><font color=\"#0000FF\">" . $c1 . ". งาน " . $rs2['project'] . "</font>&nbsp;";
                    $setok = 2;
                }
                $word .= "<br>";
                if (($rs2['pro1'] != "0") or ($rs2['pro2'] != "0") or ($rs2['set_voucher'] != "0") or ($rs2['tfex_voucher'] != "0") or ($rs2['double_voucher'] != "0") or ($rs2['b2b_voucher'] != "0")) {
                    $word .= "ใช้สิทธิ์ <font color=\"#0000FF\">";
                    if ($rs2['pro1'] != "0") {
                        $word .= "ลงทุนสม่ำเสมอ&nbsp;,";
                    }
                    if ($rs2['pro2'] != "0") {
                        $word .= "pro2 - " . $rs2['pro2_text'] . "&nbsp;,";
                    }
                    if ($rs2['set_voucher'] != "0") {
                        $word .= "SET Voucher&nbsp;,";
                    }
                    if ($rs2['tfex_voucher'] != "0") {
                        $word .= "TFEX Voucher&nbsp;,";
                    }
                    if ($rs2['double_voucher'] != "0") {
                        $word .= "Voucher เปิดบัญชีคู่&nbsp;,";
                    }
                    if ($rs2['b2b_voucher'] != "0") {
                        $word .= "Voucher AomWise&nbsp;,";
                    }
                    $word .= "</font>";
                }

                if ((($rs2['stock'] == "0") && ($rs2['futures'] == "0")) && ($rs2['mf'] == "0")) {
                    //ไม่ทำอะไร
                } else {
                    $word .= "เปิดบัญชี ";
                    if ($rs2['stock'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color=\"#0000FF\"> หุ้น </font> ";
                        $con = 1;
                    }
                    if ($rs2['futures'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color=\"#0000FF\"> อนุพันธ์ </font> ";
                        $con = 1;
                    }
                    if ($rs2['mf'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color=\"#0000FF\"> กองทุนรวมออนไลน์ </font> ";
                        if ($rs2['mf_volume'] != "") {
                            $word .= " มูลค่าลงทุน " . number_format($rs2['mf_volume']);
                        }
                        $con = 1;
                    }
                }
                $c1++;
                if ($rs2['sec_name'] == "") {
                    //ไม่เพิ่ม
                } else {
                    $word .= " ที่ <font color=\"#0000FF\">  " . $rs2['sec_name'] . " </font>";
                }
                if ($rs2['date'] == "") {
                    //no
                } else {
                    $word .= " วันที่  <font color=\"#0000FF\"> " . $rs2['date'] . "</font>";
                }
                if ($rs2['time'] == "") {
                    //no
                } else {
                    $word .= " เวลา  <font color=\"#0000FF\"> " . $rs2['time'] . "</font>";
                }
            }
        } else {
            $word = "รหัสบัตรประชาชนนี้ไม่เคยถูกใช้แลกของรางวัลในหุ้นหรืออนุพันธ์";

            $sql_f = "select count(*) as `fund` from `funds` where `id` = '" . $id . "' and `project` = '" . $project . "' ORDER BY `id_id` DESC";
            try {
                $getFundCountQuery = $pdo->query($sql_f);
            }
            catch (PDOExeption $e) {
                die("Query failed: " . $e . getMessage());
            }
            $rs_f = $getFundCountQuery->fetchColumn();
            if ($rs_f != 0) {
                //$word .= "<br><font color=\"#FF0000\">แต่เคยแลกของในกองทุนภายในงานนี้แล้ว มูลค่าลงทุน ".number_format($howmuch)."</font><br>";
                $word .= "<br><font color=\"#0000FF\">แต่เคยแลกของในกองทุนภายในงานนี้แล้ว</font>";
            }
            $setok = 1;

        }

    } else if ($what == "funds") {
        $sql = "select count(*) from `funds` where `id` = '" . $id . "' and `pro` = 'pro' ORDER BY `id_id` DESC";
        try {
            $getCountQuery = $pdo->query($sql);
        }
        catch (PDOExeption $e) {
            die("Query failed: " . $e . getMessage());
        }
        $rs = $getCountQuery->fetchColumn();
        if ($rs != 0) {
            $word = "รหัสบัตรประชาชนนี้เคยแลกของแล้ว <font color=\"#FF0000\">" . $rs . "</font> ครั้ง<br>ตามชื่องานต่อไปนี้";
            $c2   = 1;
            $sql2 = "select * from `funds` where `id` = '" . $id . "' and `pro` = 'pro' ORDER BY `id_id` DESC";
            try {
                $getAllFundQuery = $pdo->query($sql2);
            }
            catch (PDOExeption $e) {
                die("Query failed: " . $e . getMessage());
            }
            $countResult = $getAllFundQuery->fetchAll();
            foreach ($countResult as $rs2) {
                $con = 0;
                if ($rs2['project'] == $project) {
                    $word .= "<br><font color=\"#FF0000\">" . $c2 . ". งาน " . $rs2['project'] . "</font>";
                    $setok = 2;
                } else {
                    $word .= "<br><font color=\"#0000FF\">" . $c2 . ". งาน " . $rs2['project'] . "</font>";
                    $setok = 2;
                }
                $word .= "<br>";
                if ((((((($rs2['eq'] == "0") && ($rs2['fix'] == "0")) && ($rs2['mix'] == "0")) && ($rs2['fif'] == "0")) && ($rs2['prop'] == "0")) && ($rs2['ltf'] == "0")) && ($rs2['rmf'] == "0") && ($rs2['other'] == "0")) {
                    //ไม่ทำอะไร
                } else {
                    //หุ้น >> Thai ESG
                    //LTF >> SSF
                    $word .= " โดยลงทุนในกองทุน ";
                    if ($rs2['eq'] == "1") {
                        $word .= "<font color='blue'>Thai EQ</font> ";
                        $con = 1;
                    }
                    if ($rs2['fix'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color='blue'>FIX</font> ";
                        $con = 1;
                    }
                    if ($rs2['mix'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color='blue'>MIX</font> ";
                        $con = 1;
                    }
                    if ($rs2['fif'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color='blue'>FIF</font> ";
                        $con = 1;
                    }
                    if ($rs2['prop'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color='blue'>Prop.</font> ";
                        $con = 1;
                    }
                    if ($rs2['ltf'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color='blue'>SSF</font> ";
                        $con = 1;
                    }
                    if ($rs2['esg'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color='blue'>Thai ESG</font> ";
                        $con = 1;
                    }
                    if ($rs2['rmf'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color='blue'>RMF</font> ";
                        $con = 1;
                    }
                    if ($rs2['other'] == "1") {
                        if ($con == 1) {
                            $word .= ",";
                            $con = 0;
                        }
                        $word .= "<font color='blue'>อื่น ๆ</font> ";
                        $con = 1;
                    }
                }
                $c2++;
                if ($rs2['howmuch'] == "") {
                    //ไม่เพิ่ม
                } else {
                    $howmuch = number_format($rs2['howmuch']);
                    if ($howmuch != "-") {
                        $word .= " ปกติ  " . "<font color='blue'>" . $howmuch . "</font>" . " บาท";
                    }
                }
                if ($rs2['howmuch_dca'] == "") {
                    //ไม่เพิ่ม
                } else {
                    $howmuch_dca = number_format($rs2['howmuch_dca']);
                    if ($howmuch_dca != "-") {
                        $word .= "  DCA  " . "<font color='blue'>" . $howmuch_dca . "</font>" . " บาท";
                    }
                }
                if ($rs2['asset_name'] == "") {
                    //ไม่เพิ่ม
                } else {
                    $word .= " ที่   " . "<font color='blue'>" . $rs2['asset_name'] . "</font>";
                }
                if ($rs2['date'] == "") {
                    //no
                } else {
                    $word .= " วันที่ " . "<font color='blue'>" . $rs2['date'] . "</font>";
                }
                if ($rs2['time'] == "") {
                    //no
                } else {
                    $word .= " เวลา " . "<font color='blue'>" . $rs2['time'] . "</font>";
                }
            }
        } else {
            $sql    = "select count(*) from `set` where `id` = '" . $id . "' and `project` = '" . $project . "' ORDER BY `id_id` DESC";
            try {
                $getFundCountQuery = $pdo->query($sql);
            }
            catch (PDOExeption $e) {
                die("Query failed: " . $e . getMessage());
            }
            $rs = $getFundCountQuery->fetchColumn();
            $word = "รหัสบัตรประชาชนนี้ไม่เคยถูกใช้แลกของรางวัลในกองทุนรวม<br/>";
            if ($rs > 0) {
                $word .= "<font color=\"#FF0000\">แต่เคยแลกของในหุ้นหรืออนุพันธ์ภายในงานนี้แล้ว</font><br/>";
                $sql_mf    = "select * from `set` where `id` = '" . $id . "' and `project` = '" . $project . "' and `mf` = '1' ORDER BY `id_id` DESC";
                try {
                    $getFundQuery = $pdo->query($sql_mf);
                }
                catch (PDOExeption $e) {
                    die("Query failed: " . $e . getMessage());
                }
                $howmuch = 0;
                $fundResults = $getFundQuery->fetchAll();
                $countFundResults = count($fundResults);
                if($countFundResults > 0){
                    foreach ($fundResults as $rs2) {
                        $howmuch = $howmuch + (int)$rs2['mf_volume'];
                    }
                    if($howmuch > 0){
                        $word .= "<font color=\"#FF0000\">และใช้สิทธิ์เปิดบัญชีกองทุนรวมออนไลน์แล้ว มูลค่าลงทุน " . number_format($howmuch) . "</font><br>";
                    }else{
                        $word .= "<font color=\"#FF0000\">และใช้สิทธิ์เปิดบัญชีกองทุนรวมออนไลน์แล้ว</font><br>";
                    }
                }
            }
            $setok = 1;
        }
    }
    if ($setok == 1) {
        echo "<div>";
    }
    echo "" . $word . "<br>";
    if ($setok == 1) {
        /*echo"
        <p style='text-align:center;'><input type=\"submit\" name=\"add\" value=\"บันทึกเพิ่ม\" onclick=\"document.getElementById('redemption').submit();\"></div></p>";*/
    }
    if ($setok == 2) {
        /*echo"
        <p style='text-align:center;'><input type=\"submit\" name=\"add\" value=\"บันทึกเพิ่ม\" onclick=\"document.getElementById('redemption').submit();\"></div></p>";*/
    }

} else {
}
?>