<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if ($_SESSION['admin_username'] == "") {
    header("Location: index.php");

}

if (isset($_POST['edit_voucher'])) {
    if (($_POST['voucher_name'] == "") || ($_POST['prefix'] == "") || ($_POST['increment_type'] == "") || ($_POST['subfix'] == "") || ($_POST['total_quantity'] == "") || ($_POST['in_stock'] == "") || ($_POST['order'] == "") || ($_POST['project'] == "") || ($_POST['date'] == "") || ($_POST['group'] == "")) {
?>

    <script type="text/javascript">

    alert("กรุณากรอกข้อมูลให้ครบถ้วน");

    </script>

    <?php

    } else {
        try {
            $pdo->beginTransaction();
            $pdoPrepareInsert = $pdo->prepare("
        UPDATE `voucher` SET `voucher_name` = :voucher_name, `prefix` = :prefix, `increment_type` =  :increment_type, `subfix` =  :subfix, `total_quantity` =  :total_quantity, `in_stock` =  :in_stock , `order` =  :order, `project` =  :project, `date` =  :date, `group` =  :group WHERE  `voucher`.`voucher_id` = :voucher_id
        ");
            $pdoPrepareInsert->execute(array(
                ":voucher_name" => $_POST['voucher_name'],
                ":prefix" => $_POST['prefix'],
                ":increment_type" => $_POST['increment_type'],
                ":subfix" => $_POST['subfix'],
                ":total_quantity" => $_POST['total_quantity'],
                ":in_stock" => $_POST['in_stock'],
                ":order" => $_POST['order'],
                ":project" => $_POST['project'],
                ":date" => $_POST['date'],
                ":group" => $_POST['group'],
                ":voucher_id" => $_GET['voucher_id']
            ));
            $pdo->commit();
        }
        catch (Exception $e) {
            $pdo->rollback();
            throw $e;
        }
        if (isset($e)) {
            echo "<script type=\"text/javascript\">
        alert(\"ไม่สามารถแก้ไขข้อมูลได้\");
        </script>";
            echo $e->getMessage();
        } else {
            echo "<script type=\"text/javascript\">
        alert(\"แก้ไข Voucher เรียบร้อยแล้ว\");
        </script>";
        }

    }

}

?>

<html>

<head>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td>

            <br><div align="center">Admin@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>แก้ไข Voucher</div><br>



          </td>

        </tr>

        <tr>

          <td>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <form action="<?php
echo htmlentities($_SERVER['PHP_SELF'])."?voucher_id=".$_GET['voucher_id'];
?>" method="POST">

              <tr valign="middle" height="25">

                <td align="right" width="33%">Voucher ของ</td>

                <td width="2%" align="center">:</td>

                <td width="65%">

                  <select name="voucher_name">

<?php

$sql = "select * from `voucher` where `voucher_id` = '".$_GET['voucher_id']."'";

try {
    $getQuery = $pdo->query($sql);
}
catch (PDOExeption $e) {
    die("Query failed: " . $e . getMessage());
}
$rs = $getQuery->fetch();

$voucher_array = array(
    "Central",
    "Lotus",
    "Robinson",
    "Paragon"
);

$select = array(
    "",
    "",
    "",
    ""
);

$loop = 0;

foreach ($voucher_array as $value) {
    if ($rs['voucher_name'] == $value) {
        $select[$loop] = "selected";

    } else {
        $select[$loop] = "";

        //Do nothing;

    }

    $loop++;

}



echo <<<SELECTBOX

          <option value="Central" $select[0]>Central</option>

          <option value="Lotus" $select[1]>Lotus</option>

          <option value="Robinson" $select[2]>Robinson</option>

          <option value="Paragon" $select[3]>Paragon</option>

SELECTBOX
    ;
?>



                  </select>

                </td>

              </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">หมายเลขชุดหน้า</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="prefix" maxlength="20" size="20" value="<?= $rs['prefix'] ?>">

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">การเพิ่มขึ้น</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%">

                  <select name="increment_type">

<?php

$type_array = array(
    "manual",
    "auto"
);

$select = array(
    "",
    ""
);

$loop = 0;

foreach ($type_array as $value) {
    if ($rs['increment_type'] == $value) {
        $select[$loop] = "selected";

    } else {
        $select[$loop] = "";

        //Do nothing;

    }

    $loop++;

}

echo <<<SELECTBOX

          <option value="manual" $select[0]>คีย์มือ (เลขไม่เรียง)</option>

          <option value="auto" $select[1]>อัตโนมัติ (เลขเรียง)</option>

SELECTBOX
    ;
?>



                  </select>

                  </td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">ถ้าเรียง เริ่มต้นที่</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="subfix" maxlength="20" size="20" value="<?= $rs['subfix'] ?>">

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">จำนวนทั้งหมด (ใบ)</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="total_quantity" maxlength="5" size="5" value="<?= $rs['total_quantity'] ?>">

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">จำนวนที่เหลือ (ใบ)</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="in_stock" maxlength="5" size="5" value="<?= $rs['in_stock'] ?>">

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">ลำดับที่</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="order" maxlength="2" size="5" value="<?= $rs['order'] ?>">

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                <td align="right" width="33%">Voucher ของ</td>

                <td width="2%" align="center">:</td>

                <td width="65%">

                  <select name="project">

<?php



$sql2 = "SELECT `project` FROM `project` WHERE `type` = 'set' AND `status` = 'Y'";
try {
    $getQuery2 = $pdo->query($sql2);
} catch (PDOException $e) {
    die("Query failed: " . $e->getMessage());
}

// Fetch all rows to iterate over them
$results2 = $getQuery2->fetchAll(PDO::FETCH_ASSOC);

// Assume $rs['project'] is defined elsewhere; replace it with your actual variable for comparison
foreach ($results2 as $rs2) {
  if (isset($rs['project']) && $rs['project'] == $rs2['project']) {
      echo "<option value='" . $rs2['project'] . "' selected>" . $rs2['project'] . "</option>";
  } else {
      echo "<option value='" . $rs2['project'] . "'>" . $rs2['project'] . "</option>";
  }
}

?>

                </select>

                </td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">Date</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="date" maxlength="10" size="10" value="<?= $rs['date']; ?>">eg. 18/05/2530

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">Group</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%">

                  <select name="group">

<?php

$type_array = array(
    "set",
    "tfex",
    "double",
    "other"
);

$select = array(
    "",
    "",
    "",
    ""
);

$loop = 0;

foreach ($type_array as $value) {
    if ($rs['group'] == $value) {
        $select[$loop] = "selected";

    } else {
        $select[$loop] = "";

        //Do nothing;

    }

    $loop++;

}

echo <<<SELECTBOX

          <option value="set" $select[0]>set</option>

          <option value="tfex" $select[1]>tfex</option>

          <option value="double" $select[2]>double</option>

          <option value="other" $select[3]>other</option>

SELECTBOX
    ;
?>



                  </select>

                  </td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center">

                      <input type="submit" name="edit_voucher" value="แก้ไข Voucher">

                    </div>

                  </td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center"></div><br>

                  </td>

                </tr>

              </form>

            </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - edit voucher</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

