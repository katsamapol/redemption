<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if ($_SESSION['admin_username'] == "") {
    header("Location: index.php");

}

if (isset($_POST['del_project'])) {
    if ($_POST['project'] != "") {
        try {
            $pdo->beginTransaction();
            $pdoPrepareInsert = $pdo->prepare("DELETE FROM `project` where `project` = :project");
            $pdoPrepareInsert->execute(array(
                ":project" => $_POST['project']
            ));
            $pdo->commit();
        }
        catch (Exception $e) {
            $pdo->rollback();
            throw $e;
        }
        if (isset($e)) {
            echo "<script type=\"text/javascript\">
alert(\"กรุณาเลือกงานด้วยค่ะ\");
</script>";
            echo $e->getMessage();
        } else {
            echo "<script type=\"text/javascript\">
alert(\"ลบงานเรียบร้อยแล้วค่ะ\");
</script>";
        }
    }
}

?>

<html>

<head>

<script src="ajaxad.js"></script>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td>

            <div align="center">Admin@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>

              ลบงาน

            </div><br>

            </td>

        </tr>

        <tr>

          <td>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">



                <tr valign="middle" height="25">

                  <td align="right" width="33%">ประเภทของงาน</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%">

                    <select name="what" id="what" onchange="showProject(this.value, document.getElementById('year').value)">

            <option value=""></option>

                      <option value="set">SET</option>

                      <option value="funds">FUNDS</option>

                    </select>

                  </td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%"> ปีของงาน</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                    <select name="year" id="year" onChange="showProject(document.getElementById('what').value, this.value)">

          <option value=""></option>

                      <?php

$year_begin = 2009;

$year_end = date("Y") + 10;

for ($i = $year_begin; $i <= $year_end; $i++) {
    $y = $i + 543;

    echo "<option value=\"$y\">" . ($i + 543) . "</option>";

}

?>

                    </select>

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%"> ชื่อของงาน</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%">



                  </td>

                </tr>

                <tr>

                  <td colspan="3">                     <div align="center" id="txtHint"><b>เลือกปีและประเภทของงานที่ต้องการ<br>

                      เพื่อแสดงรายการงานที่เลือก</b><br>



                    </div>

                  </td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center"></div>

                  </td>

                </tr>



            </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - del project</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

