<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if ($_SESSION['admin_username'] == "") {
    header("Location: index.php");

}

if (isset($_POST['admin_edit'])) {
    $sql = "select * from `admin` where `username` = '".$_SESSION['admin_username']."'";
    try {
        $getQuery = $pdo->query($sql);
    }
    catch (PDOExeption $e) {
        die("Query failed: " . $e . getMessage());
    }
    $rs = $getQuery->fetch();
    if($_POST['npassword'] != ""){
      if ($rs['password'] == $_POST['opassword']) {
          try {
              $pdo->beginTransaction();
              $pdoPrepareInsert = $pdo->prepare("
                UPDATE `admin` SET `password` = :npassword where `username` = :admin_username
              ");
              $pdoPrepareInsert->execute(array(
                  ":npassword" => (isset($_POST['npassword']) ? $_POST['npassword'] : false),
                  ":admin_username" => $_SESSION['admin_username']
              ));
              $pdo->commit();
          }
          catch (Exception $e) {
              $pdo->rollback();
              throw $e;
          }
          if (isset($e)) {
              echo "Cannot update password, Please contact Administrator<br/>";
              echo $e->getMessage();
          } else {
              echo "<script type=\"text/javascript\">
                  alert(\"แก้ไขรหัสผ่านเรียบร้อยแล้วค่ะ\");
                  </script>";
          }
      } else {
          echo "<script type=\"text/javascript\">
                  alert(\"รหัสผ่านเก่าผิดพลาด\");
                  </script>";

      }
    }else{
      echo "<script type=\"text/javascript\">
                  alert(\"กรุณาระบุรหัสผ่านใหม่\");
                  </script>";
    }

}

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td>

            <div align="center">Admin edit page<br>

              Redemption point</div>

          </td>

        </tr>

        <tr>

          <td \>



            <table width="100%" border="0" cellspacing="0" cellpadding="0">

              <form action="<?php
echo htmlentities($_SERVER['PHP_SELF']);
?>" method="POST">

                <tr>

                  <td align="right" width="43%">username</td>

                  <td align="center" width="3%">:</td>

                  <td width="54%">&nbsp;

                    <?= $_SESSION['admin_username'] ?>

                  </td>

                </tr>

                <tr>

                  <td align="right" width="43%">old-password</td>

                  <td align="center" width="3%">:</td>

                  <td width="54%">

                    <input type="password" name="opassword">

                  </td>

                </tr>

                <tr>

                  <td align="right" width="43%">new-password</td>

                  <td align="center" width="3%">:</td>

                  <td width="54%">

                    <input type="password" name="npassword">

                  </td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center">

                      <input type="submit" name="admin_edit" value="edit">

                    </div>

                  </td>

                </tr>

              </form>

            </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - edit</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>