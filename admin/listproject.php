<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if (isset($_GET['change'])) {
    try {
        $pdo->beginTransaction();
        $pdoPrepareInsert = $pdo->prepare("UPDATE `project` SET `status` = :change where `id` = :idCard");
        $pdoPrepareInsert->execute(array(
            ":change" => $_GET['change'],
            ":idCard" => $_GET['id']
        ));
        $pdo->commit();
    }
    catch (Exception $e) {
        $pdo->rollback();
        throw $e;
    }
    if (isset($e)) {
        echo "<script type=\"text/javascript\">
alert(\"ไม่สามาถปรับปรุงข้อมูลได้\");
</script>";
        echo $e->getMessage();
    } else {
        echo "<script type=\"text/javascript\">
alert(\"ปรับปรุงข้อมูลแล้ว\");
</script>";
    }

} else {
    $text = "";

}

if (isset($_POST['sub_sort'])) {
    try {
        $pdo->beginTransaction();
        $pdoPrepareInsert = $pdo->prepare("UPDATE `project` SET `sort` = :sort where `id` = :idCard");
        $pdoPrepareInsert->execute(array(
            ":sort" => $_POST['sort'],
            ":idCard" => $_POST['id']
        ));
        $pdo->commit();
    }
    catch (Exception $e) {
        $pdo->rollback();
        throw $e;
    }
    if (isset($e)) {
        echo "<script type=\"text/javascript\">
alert(\"ไม่สามาถปรับปรุงข้อมูลได้\");
</script>";
        echo $e->getMessage();
    } else {
        echo "<script type=\"text/javascript\">
alert(\"ปรับปรุงข้อมูลแล้ว\");
</script>";
    }

} else {
    $text = "";

}

?>

<html>

<head>

<title>รายการ บล./บลจ. ที่มีอยู่</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid black;">

        <tr>

          <td>

            <br><div align="center">Admin2@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>รายการ บล./บลจ. ที่มีอยู่

			  <br><br><?= $text ?></div><br>

          </td>

        </tr>

		<tr>

			<td align=center>

				<form action="listproject.php" method="post">

				<?php

$sql = "select distinct(`year`) from `project`";
try {
    $getQuery = $pdo->query($sql);
}
catch (PDOExeption $e) {
    die("Query failed: " . $e . getMessage());
}
$results = $getQuery->fetchAll();

?>

				year : <select name="year">

				<option value=""></option>

				<?php

foreach ($results as $rs) {
    if ($_REQUEST['year'] == $rs['year']) {
        $selected = "selected";

    } else {
        $selected = "";

    }

    echo "<option value='" . $rs['year'] . "' " . $selected . ">" . $rs['year'] . "</option>";

}

?>

				</select>

				<?php

if (isset($_REQUEST['type']) == "set") {
    $s = "";

    $set_s = "selected";

    $funds_s = "";

} else if (isset($_REQUEST['type']) == "funds") {
    $s = "";

    $set_s = "";

    $funds_s = "selected";

} else {
    $s = "selected";

    $set_s = "";

    $funds_s = "";

}

?>

				type : <select name="type">

				<option value="" <?= $s ?>></option>

				<option value="set" <?= $set_s ?>>set</option>

				<option value="funds" <?= $funds_s ?>>funds</option>

				</select>

				<input type="submit" name="search" value="search">

				</form>

			</td>

		</tr>

        <tr>

          <td>

			<?php

if (isset($_REQUEST['year']) && isset($_REQUEST['type'])) {
    $sqlX = "select * from `project` where `year` = '" . $_REQUEST['year'] . "' and `type` = '" . $_REQUEST['type'] . " ' order by `id` DESC";
    try {
        $getQueryX = $pdo->query($sqlX);
    }
    catch (PDOExeption $e) {
        die("Query failed: " . $e . getMessage());
    }
    $resultXs = $getQueryX->fetchAll();

    $i = 1;

?>

			<table cellspacing=0 cellpadding=3 border=1 align=center>

			<?php

    foreach ($resultXs as $rs) {
        if ($rs['status'] == 'Y') {
            $status_show = "เปิดใช้";

            $link = htmlentities($_SERVER['PHP_SELF']) . "?change=N&id=" . $rs['id'] . "&year=" . $_REQUEST['year'] . "&type=" . $_REQUEST['type'];

        } else {
            $status_show = "<font color=red>ปิด</font>";

            $link = htmlentities($_SERVER['PHP_SELF']) . "?change=Y&id=" . $rs['id'] . "&year=" . $_REQUEST['year'] . "&type=" . $_REQUEST['type'];

        }

?>

			<tr><td><?= $i ?></td><td><?= $rs['project'] ?></td><td>

			<form action="listproject.php" method="post" style="margin:0px;padding:0px;">

			<input type="text" name="sort" value="<?= $rs['sort'] ?>" style="width:30px;">

			<input type="hidden" name="type" value="<?= $rs['type'] ?>">

			<input type="hidden" name="year" value="<?= $rs['year'] ?>">

			<input type="hidden" name="id" value="<?= $rs['id'] ?>">

			<input type="submit" name="sub_sort" value="go" style="width:30px;">

			</form>

			</td><td><a href="<?= $link ?>"><?= $status_show ?></a></td></tr>

			<?php
        $i++;
    }
?>

			</table>

			<?php
} //end type
?>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - list securities asset management</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

