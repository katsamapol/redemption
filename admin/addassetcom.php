<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if ($_SESSION['admin_username'] == "") {
    header("Location: index.php");

}

if (isset($_POST['add_assetcom'])) {
    if ($_POST['name'] == "") {
        $text = "<font color='red'>กรุณาใส่ชื่อของ บลจ. ด้วยค่ะ</font>";

    } else {
        $match = 0;

        $sql = "select * from `asset_com`";
        try {
            $getQuery = $pdo->query($sql);
        }
        catch (PDOExeption $e) {
            die("Query failed: " . $e . getMessage());
        }
        $results = $getQuery->fetchAll();

        foreach ($results as $rs) {
            if ($rs['name'] == $_POST['name']) {
                $text = "<font color='red'>มีชื่องานนี้อยู่แล้วในระบบค่ะ</font>";

                $match = 1;

            }

        }

        if ($match == 0) {
            try {
                $pdo->beginTransaction();
                $pdoPrepareInsert = $pdo->prepare("
                  INSERT INTO `asset_com` (`name`, `nick_name`) VALUES (:name, :nick_name)
                  ");
                $pdoPrepareInsert->execute(array(
                    ":name" => $_POST['name'],
                    ":nick_name" => $_POST['nick_name']
                ));
                $pdo->commit();
            }
            catch (Exception $e) {
                $pdo->rollback();
                throw $e;
            }
            if (isset($e)) {
                $text = "<font color='red'>ไม่สามารถเพิ่ม บลจ. ใหม่ได้ อาจเป็นเพราะมีชื่องานนี้อยู่แล้ว</font>";
                echo $e->getMessage();
            } else {
                $text = "<font color='blue'>เพิ่ม บลจ. ใหม่เรียบร้อยแล้ว</font>";
            }

        }

    }

}

?>

<html>

<head>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td>

            <br><div align="center">Admin@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>

              เพิ่ม บลจ. ใหม่<br><br><?= (isset($text)?$text:false) ?></div>

          </td>

        </tr>

        <tr>

          <td>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">

      <form action="<?php
echo htmlentities($_SERVER['PHP_SELF']);
?>" method="POST">

              <tr valign="middle" height="25">

                <td align="right" width="33%">ชื่อย่อ</td>

                <td width="2%" align="center">:</td>

                <td width="65%">

                  <input type="text" name="nick_name" maxlength="15" size="15">

                </td>

              </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%"> ชื่อของ บลจ. </td>

                  <td width="2%" align="center">:</td>

                  <td width="65%">

                    <input type="text" name="name" maxlength="100" size="50">

                    <font color="#FF0000"> *ไม่เกิน 100 ตัวอักษร</font></td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center">

                      <input type="submit" name="add_assetcom" value="เพิ่ม บลจ. ใหม่ ">

                    </div>

                  </td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center"></div>

                    <br>

                  </td>

                </tr>

              </form>

            </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - add project</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

