<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if ($_SESSION['admin_username'] == "") {
    header("Location: index.php");

}

if (isset($_POST['add_project'])) {
    if ($_POST['type'] == "") {
?>

  <script type="text/javascript">

  alert("กรุณาเลือกประเภทของงานด้วยค่ะ");

  </script>

  <?php

    } else if ($_POST['year'] == "") {
?>

  <script type="text/javascript">

  alert("กรุณาเลือกปีที่ต้องการเพิ่มด้วยค่ะ");

  </script>

  <?php

    } else if ($_POST['project'] == "") {
?>

  <script type="text/javascript">

  alert("กรุณาใส่ชื่องานด้วยค่ะ");

  </script>

  <?php

    } else {
        try {
            $pdo->beginTransaction();
            $pdoPrepareInsert = $pdo->prepare("
INSERT INTO `project` (`project`,`year`,`type`) VALUES (:project, :year, :type)
");
            $pdoPrepareInsert->execute(array(
                ":project" => $_POST['project'],
                ":year" => $_POST['year'],
                ":type" => $_POST['type']
            ));
            $pdo->commit();
        }
        catch (Exception $e) {
            $pdo->rollback();
            throw $e;
        }
        if (isset($e)) {
            echo "<script type=\"text/javascript\">
alert(\"ไม่สามารถเพิ่มงานใหม่ได้ อาจเป็นเพราะมีชื่องานนี้อยู่แล้ว\");
</script>";
            echo $e->getMessage();
        } else {
            echo "<script type=\"text/javascript\">
alert(\"เพิ่มงานใหม่เรียบร้อยแล้ว\");
</script>";
        }

    }

}

?>

<html>

<head>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td>

            <br><div align="center">Admin@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>เพิ่มงานใหม่</div><br>



          </td>

        </tr>

        <tr>

          <td>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <form action="<?php
echo htmlentities($_SERVER['PHP_SELF']);
?>" method="POST">

              <tr valign="middle" height="25">

                <td align="right" width="33%">ประเภทของงาน</td>

                <td width="2%" align="center">:</td>

                <td width="65%">

                  <select name="type">

          <option value=""></option>

          <option value="set">SET</option>

          <option value="funds">FUNDS</option>

                  </select>

                </td>

              </tr>



                <tr valign="middle" height="25">

                  <td align="right" width="33%"> ปีของงาน</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                    <select name="year">

                      <option value=""></option>

                      <?php

$year_begin = date("Y") - 1;

$year_end = date("Y") + 3;

for ($i = $year_begin; $i <= $year_end; $i++) {
    $y = $i + 543;

    echo "<option value=\"$y\">" . ($i + 543) . "</option>";

}

?>

                    </select>

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%"> ชื่อของงาน</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%">

                    <input type="text" name="project" maxlength="50" size="50">

                    <font color="#FF0000"> *ไม่เกิน 50 ตัวอักษร</font></td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center">

                      <input type="submit" name="add_project" value="เพิ่มงานใหม่ ">

                    </div>

                  </td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center"></div><br>

                  </td>

                </tr>

              </form>

            </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - add project</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

