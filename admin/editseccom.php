<?
session_start();
require "global_admin.php";
if($_GET['id'] == ""){
	header("Location :listseccom.php");
}else{
$id = $_GET['id'];
}
$text = "";
if (isset($_POST['edit_seccom'])) {
  if ($_POST['name'] == "") {
      $text = "<font color='red'>กรุณาใส่ชื่อของ บล. ด้วยค่ะ</font>";
  } else {
      $match = 0;

      // Use prepared statements to prevent SQL injection
      try {
          // Initialize the PDO object
          $pdo->beginTransaction();

          // Prepare the query to select existing entries except the current one being edited
          $sql = "SELECT * FROM sec_com WHERE id != :id";
          $stmt = $pdo->prepare($sql);
          $stmt->execute([':id' => $id]);
          $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

          // Check if the name already exists
          foreach ($results as $rs) {
              if ($rs['name'] == $_POST['name']) {
                  $text = "<font color='red'>มีชื่องานนี้อยู่แล้วในระบบค่ะ</font>";
                  $match = 1;
                  break;
              }
          }

          if ($match == 0) {
              // Prepare the update query using prepared statements
              $updateQuery = "UPDATE sec_com SET name = :name, nick_name = :nick_name WHERE id = :id";
              $stmt = $pdo->prepare($updateQuery);

              // Execute the update
              $result = $stmt->execute([
                  ':name' => $_POST['name'],
                  ':nick_name' => $_POST['nick_name'],
                  ':id' => $id
              ]);

              if (!$result) {
                  $pdo->rollBack(); // Rollback if update fails
                  $text = "<font color='red'>ไม่สามารถแก้ไข บล. ได้ อาจเป็นเพราะมีชื่องานนี้อยู่แล้ว</font>";
              } else {
                  $pdo->commit(); // Commit the transaction
                  $text = "<font color='blue'>แก้ไข บล. เรียบร้อยแล้ว</font>";
              }
          } else {
              $pdo->rollBack(); // Rollback if name already exists
          }
      } catch (Exception $e) {
          $pdo->rollBack(); // Rollback on any failure
          $text = "<font color='red'>เกิดข้อผิดพลาด: " . $e->getMessage() . "</font>";
      }
  }
}
// if(isset($_POST['edit_seccom'])){
// 	if($_POST['name'] == ""){
// 		$text = "<font color='red'>กรุณาใส่ชื่อของ บล. ด้วยค่ะ</font>";
// 	}else{
// 	$match = 0;
// 	$sql = "select * from sec_com where id != $id";
// 	$result = mysql_query($sql);
// 	while($rs = mysql_fetch_array($result)){
// 		if($rs['name'] == $_POST['name']){
// 			$text = "<font color='red'>มีชื่องานนี้อยู่แล้วในระบบค่ะ</font>";
// 			$match = 1;
// 		}
// 	}
// 	if($match == 0){
// 		$sql = "update sec_com  set name='".$_POST['name']."' ,nick_name='".$_POST['nick_name']."' where id = '".$id."'";
// 		$result = mysql_query($sql);
// 			if(!$result){
// 				$text = "<font color='red'>ไม่สามารถแก้ไข บล. ได้ อาจเป็นเพราะมีชื่องานนี้อยู่แล้ว</font>";
// 			}else{
// 				$text = "<font color='blue'>แก้ไข บล. เรียบร้อยแล้ว</font>";
// 			}
// 		}
// 	}
// }
?>
<html>
<head>
<title>แก้ไขข้อมูล บล.</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<?
try {
  // Prepare the query with a placeholder for the `id`
  $sql = "SELECT * FROM sec_com WHERE id = :id";
  $stmt = $pdo->prepare($sql);

  // Execute the query with the `id` value
  $stmt->execute([':id' => $id]);

  // Fetch the result as an associative array
  $rs = $stmt->fetch(PDO::FETCH_ASSOC);

  // Check if a result was found
  if ($rs) {
      // Do something with the result, for example:
      // echo $rs['name'];
  } else {
      // No result found
      echo "No records found with id: " . htmlspecialchars($id);
  }
} catch (PDOException $e) {
  // Handle any database-related errors
  echo "Error: " . $e->getMessage();
}
?>
<body bgcolor="#FFFFFF" text="#000000">
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td height="1" background="images/point.jpg"></td>
    <td height="1" background="images/point.jpg"></td>
    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>
  </tr>
  <tr>
    <td width="1" background="images/point.jpg"></td>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid black;">
        <tr>
          <td>
            <br><div align="center">Admin@Redemption point<br>
              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>แก้ไขข้อมูล บล.<br><br><?=$text?></div>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<form action="<?$PHP_SELF?>" method="POST">
              <tr valign="middle" height="25">
                <td align="right" width="33%">ชื่อย่อ</td>
                <td width="2%" align="center">:</td>
                <td width="65%">
                  <input type="text" name="nick_name" maxlength="15" size="15" value="<?=$rs['nick_name']?>">
                </td>
              </tr>
                <tr valign="middle" height="25">
                  <td align="right" width="33%"> ชื่อของ บล. </td>
                  <td width="2%" align="center">:</td>
                  <td width="65%">
                    <input type="text" name="name" maxlength="100" size="50" value="<?=$rs['name']?>">
                    <font color="#FF0000"> *ไม่เกิน 100 ตัวอักษร</font></td>
                </tr>
                <tr>
                  <td colspan="3">
                    <div align="center">
                      <input type="submit" name="edit_seccom" value="แก้ไข บล. ">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <div align="center"></div>
                    <br>
                  </td>
                </tr>
              </form>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" background="images/shadowpoint.jpg"></td>
  </tr>
  <tr>
    <td height="1" background="images/point.jpg"></td>
    <td height="1" background="images/point.jpg"></td>
    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>
  </tr>
</table>
<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td><a href="main.php">index</a> - <a href="listseccom.php">list securities</a> - edit securities</td>
  </tr>
</table><br>
  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ
  </font></div>
</body>
</html>
