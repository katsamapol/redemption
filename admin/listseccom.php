<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if (isset($_GET['change'])) {
    try {
        $pdo->beginTransaction();
        $pdoPrepareInsert = $pdo->prepare("UPDATE `sec_com` SET `status` = :change where `id` = :idCard");
        $pdoPrepareInsert->execute(array(
            ":change" => $_GET['change'],
            ":idCard" => $_GET['id']
        ));
        $pdo->commit();
    }
    catch (Exception $e) {
        $pdo->rollback();
        throw $e;
    }
    if (isset($e)) {
        $text = "<font color=red>ไม่สามาถปรับปรุงข้อมูลได้</font>";
        echo $e->getMessage();
    } else {
        $text = "<font color=blue>ปรับปรุงข้อมูลแล้ว</font>";
    }

} else {
    $text = "";

}

?>

<html>

<head>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid black;">

        <tr>

          <td>

            <br><div align="center">Admin2@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>รายการ บล. ที่มีอยู่

      <br><br><?= (isset($text) ? $text : false) ?></div><br>

      </td>

        </tr>

        <tr>

          <td>

      <?php

$sql = "select * from `sec_com` order by `nick_name` ASC";
try {
    $getQuery = $pdo->query($sql);
}
catch (PDOExeption $e) {
    die("Query failed: " . $e . getMessage());
}
$results = $getQuery->fetchAll();

$i = 1;

?>

      <table cellspacing=0 cellpadding=3 border=1 align=center>

      <?php

foreach ($results as $rs) {
    if ($rs['status'] == 'Y') {
        $status_show = "เปิดใช้";

        $link = htmlentities($_SERVER['PHP_SELF']) . "?change=N&id=" . $rs['id'] . "#" . $rs['nick_name'];

    } else {
        $status_show = "<font color=red>ปิด</font>";

        $link = htmlentities($_SERVER['PHP_SELF']) . "?change=Y&id=" . $rs['id'] . "#" . $rs['nick_name'];

    }

?>

      <tr><td><?= $i ?></td><td id='<?= $rs['nick_name'] ?>'><?= $rs['nick_name'] ?></td><td><?= $rs['name'] ?></td><td><a href="<?= $link ?>"><?= $status_show ?></a></td><td><a href="editseccom.php?id=<?= $rs['id'] ?>">edit</a></td></tr>

      <?php

    $i++;
}
?>

      </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - list securities management</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

