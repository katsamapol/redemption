<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if ($_SESSION['admin_username'] == "") {
    header("Location: index.php");

}

if (isset($_GET['change'])) {
    try {
        $pdo->beginTransaction();
        $pdoPrepareInsert = $pdo->prepare("
        UPDATE `voucher` SET `status` = :change WHERE `voucher_id` = :voucher_id
        ");
        $pdoPrepareInsert->execute(array(
            ":change" => $_GET['change'],
            ":voucher_id" => $_GET['voucher_id']
        ));
        $pdo->commit();
    }
    catch (Exception $e) {
        $pdo->rollback();
        throw $e;
    }
    if (isset($e)) {
        $text = "<font color=red>ไม่สามาถปรับปรุงข้อมูลได้</font>";
        echo $e->getMessage();
    } else {
        $text = "<font color=blue>ปรับปรุงข้อมูลแล้ว</font>";
    }

} else {
    $text = "";

}

?>

<html>

<head>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid black;">

        <tr>

          <td>

            <br><div align="center">Admin2@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>รายการ Voucher ที่มีอยู่

      <br><br><?= (isset($text) ? $text : false) ?></div><br>

      </td>

        </tr>

        <tr>

          <td>

      <?php

$sql = "select * from `voucher` order by `voucher_id` DESC";
try {
    $getQuery = $pdo->query($sql);
}
catch (PDOExeption $e) {
    die("Query failed: " . $e . getMessage());
}
$results = $getQuery->fetchAll();

$i = 1;

?>

      <table cellspacing=0 cellpadding=3 border=1 align=center>

      <tr>

        <td>ลำดับที่</td><td>ประเภท</td><td>เลขนำ</td><td>การเพิ่มขึ้น</td><td>เลขตาม</td><td>จำนวน</td><td>เหลืออยู่</td><td>แก้ไข</td><td>สถานะ</td>

      </tr>

      <?php

foreach ($results as $rs) {
    if ($rs['status'] == 'Y') {
        $status_show = "เปิดใช้อยู่";

        $link = htmlentities($_SERVER['PHP_SELF']) . "?change=N&voucher_id=" . $rs['voucher_id'] . "#" . $rs['prefix'];

    } else {
        $status_show = "<font color=red>ไม่ใช้</font>";

        $link = htmlentities($_SERVER['PHP_SELF']) . "?change=Y&voucher_id=" . $rs['voucher_id'] . "#" . $rs['prefix'];

    }

    if ($rs['increment_type'] == "manual") {
        $increment_type = "คีย์มือ";

    } else if ($rs['increment_type'] == "auto") {
        $increment_type = "อัตโนมัติ";

    } else {
        $increment_type = "";

    }

?>

<?php

    echo <<<EOB
      <tr>
      <td>$rs[voucher_id]</td>
      <td>$rs[voucher_name]</td>
      <td id="$rs[prefix]">$rs[prefix]</td>
      <td>$increment_type</td>
      <td>$rs[subfix]</td>
      <td>$rs[total_quantity]</td>
      <td>$rs[in_stock]</td>
      <td><a href="editvoucher.php?voucher_id=$rs[voucher_id]">edit</a></td>
      <td><a href="$link">$status_show</a></td>
      </tr>
EOB;
?>
      <?php

    $i++;
}
?>

      </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - list voucher</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

