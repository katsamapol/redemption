<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if ($_SESSION['admin_username'] == "") {
    header("Location: index.php");

}

if (isset($_POST['del_seccom'])) {
    if ($_POST['name'] != "") {
        try {
            $pdo->beginTransaction();
            $pdoPrepareInsert = $pdo->prepare("
              DELETE FROM `sec_com` where `name` = :name
              ");
            $pdoPrepareInsert->execute(array(
                ":name" => $_POST['name']
            ));
            $pdo->commit();
        }
        catch (Exception $e) {
            $pdo->rollback();
            throw $e;
        }
        if (isset($e)) {
            $text = "<font color='red'>กรุณาเลือก บล. ด้วยค่ะ</font>";
            echo $e->getMessage();
        } else {
            $text = "<font color='blue'>ลบ บล. เรียบร้อยแล้วค่ะ</font>";
        }
    }
}

?>

<html>

<head>

<script src="ajaxad.js"></script>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td>

            <br><div align="center">Admin@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg">

        <br>ลบ บล.<br><br><?= (isset($text)?$text:false) ?>

            </div>

            </td>

        </tr>

        <tr>

          <td>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">

      <form action="<?php
echo htmlentities($_SERVER['PHP_SELF']);
?>" method="POST">

              <tr valign="middle" height="25">

                <td align="right">

                  <div align="center">รายการ บล. ทั้งหมด :

                    <select name="name">

                      <option value=""></option>

                      <?php

$sql = "select * from `sec_com`";

try {
    $getQuery = $pdo->query($sql);
}
catch (PDOExeption $e) {
    die("Query failed: " . $e . getMessage());
}
$results = $getQuery->fetchAll();

foreach ($results as $rs) {
?>

                      <option value="<?= $rs['name'] ?>">

                      <?= $rs['name'] ?>

                      </option>

                      <?php

}

?>

                    </select>

                  </div>

                </td>

              </tr>

              <tr>

                <td>

                  <div align="center" id="txtHint">

                    <input type="submit" name="del_seccom" value="ลบ บล. ที่เลือก">

                  </div>

                </td>

              </tr>

              <tr>

                <td>

                  <div align="center"></div><br>

                </td>

              </tr>

        </form>

            </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - delete security company</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

