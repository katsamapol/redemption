<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if ($_SESSION['admin_username'] == "") {
    header("Location: index.php");

}

if (isset($_POST['add_voucher'])) {
    if (($_POST['voucher_name'] == "") || ($_POST['prefix'] == "") || ($_POST['increment_type'] == "") || ($_POST['subfix'] == "") || ($_POST['total_quantity'] == "") || ($_POST['in_stock'] == "") || ($_POST['order'] == "") || ($_POST['project'] == "") || ($_POST['date'] == "") || ($_POST['group'] == "")) {
?>

    <script type="text/javascript">

    alert("กรุณากรอกข้อมูลให้ครบถ้วน");

    </script>

    <?php

    } else {
        try {
            $pdo->beginTransaction();
            $pdoPrepareInsert = $pdo->prepare("
                  INSERT INTO `voucher` (`voucher_name`, `prefix`, `increment_type`, `subfix`, `total_quantity`, `in_stock`, `order`, `project`, `date`, `group`) VALUES (:voucher_name, :prefix, :increment_type, :subfix, :total_quantity, :in_stock, :order, :project, :date, :group)
                  ");
            $pdoPrepareInsert->execute(array(
                ":voucher_name" => $_POST['voucher_name'],
                ":prefix" => $_POST['prefix'],
                ":increment_type" => $_POST['increment_type'],
                ":subfix" => $_POST['subfix'],
                ":total_quantity" => $_POST['total_quantity'],
                ":in_stock" => $_POST['in_stock'],
                ":order" => $_POST['order'],
                ":project" => $_POST['project'],
                ":date" => $_POST['date'],
                ":group" => $_POST['group']
            ));
            $pdo->commit();
        }
        catch (Exception $e) {
            $pdo->rollback();
            throw $e;
        }
        if (isset($e)) {
            echo "<script type=\"text/javascript\">
                alert(\"ไม่สามารถเพิ่ม Voucher ใหม่ได้ อาจเป็นเพราะมีหมายเลข voucher นี้อยู่แล้ว\");
                </script>";
            echo $e->getMessage();
        } else {
            echo "<script type=\"text/javascript\">
                alert(\"เพิ่ม Voucher เรียบร้อยแล้ว\");
                </script>";
        }

    }

}

?>

<html>

<head>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td>

            <br><div align="center">Admin@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>เพิ่ม Voucher ใหม่</div><br>



          </td>

        </tr>

        <tr>

          <td>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <form action="<?php
echo htmlentities($_SERVER['PHP_SELF']);
?>" method="POST">

              <tr valign="middle" height="25">

                <td align="right" width="33%">Voucher ของ</td>

                <td width="2%" align="center">:</td>

                <td width="65%">

                  <select name="voucher_name">

          <option value=""></option>

          <option value="Central">Central</option>

          <option value="Lotus">Lotus</option>

          <option value="Robinson">Robinson</option>

          <option value="Paragon">Paragon</option>

                  </select>

                </td>

              </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">หมายเลขชุดหน้า</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="prefix" maxlength="20" size="20">

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">การเพิ่มขึ้น</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%">

                  <select name="increment_type">

          <option value=""></option>

          <option value="manual">คีย์มือ (เลขไม่เรียง)</option>

          <option value="auto">อัตโนมัติ (เลขเรียง)</option>

                  </select>

                  </td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">ถ้าเรียง เริ่มต้นที่</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="subfix" maxlength="20" size="20">

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">จำนวนทั้งหมด (ใบ)</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="total_quantity" maxlength="5" size="5">

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">จำนวนที่เหลือ (ใบ)</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="in_stock" maxlength="5" size="5">

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">ลำดับ (0-99)</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="order" maxlength="2" size="5">

                    </font></td>

                </tr>

              <tr valign="middle" height="25">

                <td align="right" width="33%">Voucher งาน</td>

                <td width="2%" align="center">:</td>

                <td width="65%">

                  <select name="project">

                  <option value="">กรุณาเลือก</option>

                  <?php

$sql = "select `project` from `project` where `status` = 'Y' and `type` ='set'";

try {
    $getQuery = $pdo->query($sql);
}
catch (PDOExeption $e) {
    die("Query failed: " . $e . getMessage());
}
$results = $getQuery->fetchAll();

foreach ($results as $rs) {
    echo "<option value='" . $rs['project'] . "'>" . $rs['project'] . "</option>";

}

?>

                  </select>

                </td>

              </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">Date</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%"><font color="#FF0000">

                  <input type="text" name="date" maxlength="10" size="10" value="<?= date('d') . "/" . date('m') . "/" . (date('Y') + 543); ?>">eg. 18/05/2530

                    </font></td>

                </tr>

                <tr valign="middle" height="25">

                  <td align="right" width="33%">Group</td>

                  <td width="2%" align="center">:</td>

                  <td width="65%">

                  <select name="group">

                    <option value="">กรุณาเลือก</option>

                    <option value="set">set</option>

                    <option value="tfex">tfex</option>

                    <option value="double">double</option>

                    <option value="other">other</option>

                  </select>

                  </td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center">

                      <input type="submit" name="add_voucher" value="เพิ่ม Voucher ใหม่">

                    </div>

                  </td>

                </tr>

                <tr>

                  <td colspan="3">

                    <div align="center"></div><br>

                  </td>

                </tr>

              </form>

            </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - add voucher</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

