<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../connect.inc.php");

if ($_SESSION['admin_username'] == "") {
    header("Location: index.php");

}

if ((isset($_GET["confirm"])) && (isset($_GET['what']))) {
    $sql = "delete from `" . $_GET['what'] . "` where id_id = '" . $_GET["id_id"] . "'";
    try {
        $pdo->beginTransaction();
        $pdoPrepareInsert = $pdo->prepare("
        DELETE FROM :what WHERE `id_id` = :id_id
        ");
        $pdoPrepareInsert->execute(array(
            ":what" => $_GET['what'],
            ":id_id" => $_GET['id_id']
        ));
        $pdo->commit();
    }
    catch (Exception $e) {
        $pdo->rollback();
        throw $e;
    }
    if (isset($e)) {
        $text = "<font color=red>ไม่สามาถปรับปรุงข้อมูลได้</font>";
        echo $e->getMessage();
    } else {
        $text = "<font color=blue>ลบข้อมูลเรียบร้อยแล้ว</font>";
    }

}

?>

<html>

<head>

<script src="ajaxedit1.js"></script>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td>

            <div align="center">Admin@Redemption point<br>

              <img src="../images/set.jpg"><img src="../images/center.jpg" width="5" height="100"><img src="../images/mf.jpg"><br>

              แก้ไข-ลบ ข้อมูลผู้ลงทะเบียน</div>

            <br>

            </td>

        </tr>

        <tr>

          <td>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr valign="middle" height="25">

                <td align="right" width="33%">ประเภทของงาน</td>

                <td width="2%" align="center">:</td>

                <td width="65%">

                  <select name="what" id="what" onchange="showid(this.value, document.getElementById('id').value)">

                    <option value=""></option>

                    <option value="set">SET</option>

                    <option value="funds">FUNDS</option>

                  </select>

                </td>

              </tr>

              <tr valign="middle" height="25">

                <td align="right" width="33%"> รหัสบัตรประชาชน</td>

                <td width="2%" align="center">:</td>

                <td width="65%"><font color="#FF0000">

                  <input type="id" id="id" name="textfield"  maxlength = "13" onkeyup="showid(document.getElementById('what').value, this.value)">

                  </font></td>

              </tr>

              <tr valign="middle" height="25">

                <td align="right" colspan="3">

                  </td>

              </tr>

              <tr>

                <td colspan="3">

                  <div align="center" id="txtHint"><b>เลือกประเภทของงานและกรอกรหัสบัตรประชาชน<br>

                    เพื่อแสดงข้อมูลผู้ลงทะเบียนที่ต้องการ</b><br>

                  </div>

                </td>

              </tr>

              <tr>

                <td colspan="3">

                  <br>

                </td>

              </tr>

            </table>

          </td>

        </tr>

      </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<div align="center"><table width="800" border="0" cellspacing="0" cellpadding="0">

  <tr>

      <td><a href="main.php">index</a> - select list to edit or delete</td>

  </tr>

</table><br>

  <font color="#FF0000">เมื่อทำการปิดหน้าต่างนี้ ระบบจะทำการล๊อคเอาท์อัตโนมัติ

  </font></div>

</body>

</html>

