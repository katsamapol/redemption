<?php

require_once("connect.inc.php");

require_once("auth.inc.php");

date_default_timezone_set('asia/bangkok');

header("Content-Type: text/html; charset=utf-8");

$word = "";

// foreach($_POST as $key=>$value){

//     echo $key." ".$_POST[$key]."<br>";

//     (isset($_POST[$key])?$_POST[$key]=$value:$_POST[$key]="");

// }

if (isset($_GET['pro'])) {
    $pro = $_GET['pro'];

    // Check if $pro is empty before setting session or cookie
    if (empty($pro)) {
        header("Location: choose.php");
        exit(); // Stop further execution after redirect
    }

    // Set session or cookie based on "Remember Me" status
    set_session_or_cookie("pro", $pro, isset($_COOKIE['username']));
}else{
    $pro = get_session_or_cookie('pro');
}

$what = get_session_or_cookie('what');
$project = get_session_or_cookie('project');

if (isset($_POST['back'])) {
    if ($what == "funds") {
        unset_session_or_cookie('pro');

        header("Location: choose.php");
    } else if ($what == "set") {
        unset_session_or_cookie('what');
        unset_session_or_cookie('project');

        header("Location: main.php?what=set");
    }

}

if ($what  == "funds") {
    if (!$pro) {
        header("Location: index.php");
    }
}

if (!isset($project)) {
    header("Location: index.php");
}

if (isset($_POST['new'])) {
    header("Location: add.php");
}

if (isset($_POST['reset'])) {
    header("Location: add.php?pro=" . $pro . "");
}

// set project
if (isset($_REQUEST['project'])) {
    $project = $_REQUEST['project'];
    if(isset($_COOKIE['username'])){
      set_session_or_cookie("project", $project, true);
    }else{
      set_session_or_cookie("project", $project, false);
    }
}

if (isset($_GET['what'])) {
    $what = $_GET['what'];
    if(isset($_COOKIE['username'])){
      set_session_or_cookie("what", $what, true);
    }else{
      set_session_or_cookie("what", $what, false);
    }

}

if (isset($_POST['report'])) {
    if ($what == "set") {
        header("Location: main_report.php");
    } else {
        header("Location: funds_main_report.php");
    }
}

//print_r($_POST);

//echo "<br>";

//print_r($_GET);

//echo "<br>";

//print_r($_SESSION);

//break;

function getDuplicateVoucher($str, $proj, $pdo)
{
    try {
        $getQuery = $pdo->query("SELECT count(*) FROM `set` WHERE `project` = '" . $proj . "' " . $str);
    }catch (PDOExeption $e) {
        die("Query failed: " . $e . getMessage());
    }

    $resultNumRows = $getQuery->fetchColumn();

    if ($resultNumRows > 0) {
        return $resultNumRows;
    } else {
        return 0;
    }
}

if (isset($_POST['add'])) {

    if ((isset($_POST['sec_name']) == "") && ($what == "set")) {

        $text = "<br><font color='red'>กรุณาเลือกบริษัทด้วยค่ะ</font>";



        $_POST['add'] = "";

    }

    if ((isset($_POST['asset_name']) == "") && ($what == "funds")) {

        $text = "<br><font color='red'>กรุณาเลือกบริษัทด้วยค่ะ</font>";



        $_POST['add'] = "";

    }

    if (($_POST['redeemer'] == "") || ($_POST['redeemer_name'] == "")) {

        $text = "<br><font color='red'>กรุณาระบุผู้แลกด้วยค่ะ</font>";

    }

    if (isset($_POST['id'])) {

        $dob     = $_POST['dob'];

        $mob     = $_POST['mob'];

        $yob     = $_POST['yob'];

        /*if($yob >= 2523 and $yob <= 2530){

        $new_gen = 1;

        }else{

        $new_gen = 0;

        }*/

        $bad_dob = "false";

        //echo "Before : ".$bad_dob;

        if ($dob != "" || $mob != "" || $yob != "") {

            $bad_dob = "true";

            if (($dob == 00) || ($yob == 00) || ($yob < ((date("Y")) - 120)) || ($yob > (date("Y"))) || ($dob > 31) || ($mob > 12)) {

?>

<script type="text/javascript">
alert("กรุณาระบุวัน เดือน ปีเกิด ให้ถูกต้อง");
</script>

<?php

            } else if ($dob == "") {

?>

<script type="text/javascript">
alert("กรุณาระบุวันเกิดให้ถูกต้อง");
</script>

<?php

            } else if ($mob == "") {

?>

<script type="text/javascript">
alert("กรุณาระบุเดือนเกิดให้ถูกต้อง");
</script>

<?php

            } else if ($yob == "") {

?>

<script type="text/javascript">
alert("กรุณาระบุปีเกิดให้ถูกต้อง");
</script>

<?php

            } else {

                if ($dob < 10) {
                    $dob = "0" . intval($dob);
                }

                if ($mob < 10) {
                    $mob = "0" . intval($mob);
                }

                if ($yob != "") {
                    if ($yob > date("Y")) {
                        //do nothing

                    } else {
                        $yob = $yob + 543;
                    }
                }
                $bad_dob = "false";
            }
        }

        //echo "<br/>After : ".$bad_dob;

        if (($what == "set") && ($_POST['sec_name'] != "") && ($bad_dob == "false")) {
            $update_prefix = array();

            if (isset($_POST['set_voucher_id_prefix'])) {
                $set_voucher_id  = $_POST['set_voucher_id_prefix'] . $_POST['set_voucher_id_subfix'];
                $update_prefix[] = $_POST['set_voucher_id_prefix'];

            }

            if (isset($_POST['tfex_voucher_id_prefix'])) {
                $tfex_voucher_id = $_POST['tfex_voucher_id_prefix'] . $_POST['tfex_voucher_id_subfix'];
                $update_prefix[] = $_POST['tfex_voucher_id_prefix'];
            }

            if (isset($_POST['double_voucher_id_prefix'])) {
                $double_voucher_id = $_POST['double_voucher_id_prefix'] . $_POST['double_voucher_id_subfix'];
                $update_prefix[]   = $_POST['double_voucher_id_prefix'];
            }

            if (isset($_POST['b2b_voucher_id_prefix'])) {
                $b2b_voucher_id  = $_POST['b2b_voucher_id_prefix'] . $_POST['b2b_voucher_id_subfix'];
                $update_prefix[] = $_POST['b2b_voucher_id_prefix'];
            }

            // $update_prefix     = array(

            //     $_POST['set_voucher_id_prefix'],

            //     $_POST['tfex_voucher_id_prefix'],

            //     $_POST['double_voucher_id_prefix'],

            //     $_POST['b2b_voucher_id_prefix']

            // );

            $date            = date('d') . "/" . date('m') . "/" . (date('Y') + 543);
            $time            = date('H') . ":" . date('i') . ":" . date('s');
            $set_num_rows    = 0;
            $tfex_num_rows   = 0;
            $double_num_rows = 0;
            $b2b_num_rows    = 0;

            if (isset($set_voucher_id)) {
                $search       = " and (`set_voucher_id` = '" . $set_voucher_id . "' or `tfex_voucher_id` = '" . $set_voucher_id . "' or `double_voucher_id` = '" . $set_voucher_id . "' or `b2b_voucher_id` = '" . $set_voucher_id . "')";
                $set_num_rows = getDuplicateVoucher($search, $project, $pdo);
            }
            
            if (isset($tfex_voucher_id)) {
                $search        = " and (`set_voucher_id` = '" . $tfex_voucher_id . "' or `tfex_voucher_id` = '" . $tfex_voucher_id . "' or `double_voucher_id` = '" . $tfex_voucher_id . "' or `b2b_voucher_id` = '" . $tfex_voucher_id . "')";
                $tfex_num_rows = getDuplicateVoucher($search, $project, $pdo);
            }

            if (isset($double_voucher_id)) {
                $search          = " and (`set_voucher_id` = '" . $double_voucher_id . "' or `tfex_voucher_id` = '" . $double_voucher_id . "' or `double_voucher_id` = '" . $double_voucher_id . "' or `b2b_voucher_id` = '" . $double_voucher_id . "')";
                $double_num_rows = getDuplicateVoucher($search, $project, $pdo);
            }

            if (isset($b2b_voucher_id)) {
                $search       = " and (`set_voucher_id` = '" . $b2b_voucher_id . "' or `tfex_voucher_id` = '" . $b2b_voucher_id . "' or `double_voucher_id` = '" . $b2b_voucher_id . "' or `b2b_voucher_id` = '" . $b2b_voucher_id . "')";
                $b2b_num_rows = getDuplicateVoucher($search, $project, $pdo);
            }

            if ($set_num_rows > 0 or $tfex_num_rows > 0 or $double_num_rows > 0 or $b2b_num_rows > 0) {

?>


<script type="text/javascript">
    window.onload = function() {
        showAlert("เลขที่ Voucher ซ้ำ ไม่สามารถบันทึกข้อมูลได้", "red");
    };
</script>

<?php

            } else {

                try {

                    $pdo->beginTransaction();
                    $pdoPrepareInsert = $pdo->prepare("INSERT INTO `set` (
                        `id`, `fname`, `sname`, `dob`, `mob`, `yob`, `sex`, `address`, `province`, `phone`, `mobile`, `email`, `project`, `date`, `time`, `redeemer`, `redeemer_name`, `stock`, `futures`, `mf`, `mf_volume`, `pro1`, `pro1_text`, `pro2`, `pro2_text`, `set_voucher`, `set_voucher_id`, `tfex_voucher`, `tfex_voucher_id`, `double_voucher`, `double_voucher_id`, `b2b_voucher`, `b2b_voucher_id`, `sec_name`, `reader`
                        ) VALUES ( :id, :fname, :sname, :dob, :mob, :yob, :sex, :address, :province, :phone, :mobile, :email, :project, :date, :time, :redeemer, :redeemer_name, :stock, :futures, :mf, :mf_volume, :pro1, :pro1_text, :pro2, :pro2_text, :set_voucher, :set_voucher_id, :tfex_voucher, :tfex_voucher_id, :double_voucher, :double_voucher_id, :b2b_voucher, :b2b_voucher_id, :sec_name, :reader)
                    ");

                    $pdoPrepareInsert->execute(array(
                        ":id" => (isset($_POST['id']) ? $_POST['id'] : false),
                        ":fname" => (isset($_POST['fname']) ? $_POST['fname'] : false),
                        ":sname" => (isset($_POST['sname']) ? $_POST['sname'] : false),
                        ":dob" => (isset($dob) ? $dob : false),
                        ":mob" => (isset($mob) ? $mob : false),
                        ":yob" => (isset($yob) ? $yob : false),
                        ":sex" => (isset($_POST['sex']) ? $_POST['sex'] : false),
                        ":address" => (isset($_POST['address']) ? $_POST['address'] : false),
                        ":province" => (isset($_POST['province']) ? $_POST['province'] : false),
                        ":phone" => (isset($_POST['phone']) ? $_POST['phone'] : false),
                        ":mobile" => (isset($_POST['mobile']) ? $_POST['mobile'] : false),
                        ":email" => (isset($_POST['email']) ? $_POST['email'] : false),
                        ":project" => (isset($project) ? $project : false),
                        ":date" => (isset($date) ? $date : false),
                        ":time" => (isset($time) ? $time : false),
                        ":redeemer" => (isset($_POST['redeemer']) ? $_POST['redeemer'] : false),
                        ":redeemer_name" => (isset($_POST['redeemer_name']) ? $_POST['redeemer_name'] : false),
                        ":stock" => (isset($_POST['stock']) ? $_POST['stock'] : 0),
                        ":futures" => (isset($_POST['futures']) ? $_POST['futures'] : 0),
                        ":mf" => (isset($_POST['mf']) ? $_POST['mf'] : 0),
                        ":mf_volume" => (isset($_POST['mf_volume']) ? $_POST['mf_volume'] : false),
                        ":pro1" => (isset($_POST['pro1']) ? $_POST['pro1'] : 0),
                        ":pro1_text" => (isset($_POST['pro1_text']) ? $_POST['pro1_text'] : false),
                        ":pro2" => (isset($_POST['pro2']) ? $_POST['pro2'] : 0),
                        ":pro2_text" => (isset($_POST['pro2_text']) ? $_POST['pro2_text'] : false),
                        ":set_voucher" => (isset($_POST['set_voucher']) ? $_POST['set_voucher'] : 0),
                        ":set_voucher_id" => (isset($set_voucher_id) ? $set_voucher_id : false),
                        ":tfex_voucher" => (isset($_POST['tfex_voucher']) ? $_POST['tfex_voucher'] : 0),
                        ":tfex_voucher_id" => (isset($tfex_voucher_id) ? $tfex_voucher_id : false),
                        ":double_voucher" => (isset($_POST['double_voucher']) ? $_POST['double_voucher'] : 0),
                        ":double_voucher_id" => (isset($double_voucher_id) ? $double_voucher_id : false),
                        ":b2b_voucher" => (isset($_POST['b2b_voucher']) ? $_POST['b2b_voucher'] : 0),
                        ":b2b_voucher_id" => (isset($b2b_voucher_id) ? $b2b_voucher_id : false),
                        ":sec_name" => (isset($_POST['sec_name']) ? $_POST['sec_name'] : false),
                        ":reader" => (isset($_POST['reader']) ? $_POST['reader'] : 0)
                    ));

                    foreach ($update_prefix as $value) {

                        $pdoPrepareUpdate = $pdo->prepare("UPDATE `voucher` SET `in_stock` = `in_stock`-1 WHERE `prefix` = :prefix ");

                        $pdoPrepareUpdate->execute(array(

                            ":prefix" => $value

                        ));

                    }

                    $pdo->commit();

                }

                catch (Exception $e) {

                    $pdo->rollback();

                    throw $e;

                }

                if (isset($e)) {

                    echo "Cannot add new customer, Please contact Administrator<br/>";

                    echo $e->getMessage();

                } else {

                    echo "<script type=\"text/javascript\">

                        alert(\"ลงทะเบียนแลกของเรียบร้อยแล้วค่ะ\");

                        window.location=\"add.php\";

                        </script>";

                
                    unset_session_or_cookie('pro');

                    $result = "";

                }

            }

        } else if (($what == "funds") && ($_POST['asset_name'] != "")) {

            $asset_name = $_POST['asset_name'];

            $date       = date('d') . "/" . date('m') . "/" . (date('Y') + 543);

            $time       = date('H') . ":" . date('i') . ":" . date('s');

            try {

                $pdo->beginTransaction();

                //do something

                $pdoPrepareInsert = $pdo->prepare("INSERT INTO `funds` (

                    `id`, `fname`, `sname`, `dob`, `mob`, `yob`, `sex`, `address`, `province`, `phone`, `mobile`, `email`, `project`, `date`, `time`, `redeemer`, `redeemer_name`, `eq`, `fix`, `mix`, `fif`, `prop`, `ltf`, `rmf`, `rmf_eq`, `esg`, `other`, `pro`, `new_gen`, `asset_name`, `howmuch`, `howmuch_dca`, `reader`

                    ) VALUES (:id,:fname, :sname, :dob, :mob, :yob, :sex, :address, :province, :phone, :mobile, :email, :project, :date, :time, :redeemer, :redeemer_name, :eq, :fix, :mix, :fif, :prop, :ltf, :rmf, :rmf_eq, :esg, :other, :pro, :new_gen, :asset_name, :howmuch, :howmuch_dca, :reader)

                ");

                $pdoPrepareInsert->execute(array(

                    ":id" => (isset($_POST['id']) ? $_POST['id'] : false),

                    ":fname" => (isset($_POST['fname']) ? $_POST['fname'] : false),

                    ":sname" => (isset($_POST['sname']) ? $_POST['sname'] : false),

                    ":dob" => (isset($dob) ? $dob : false),

                    ":mob" => (isset($mob) ? $mob : false),

                    ":yob" => (isset($yob) ? $yob : false),

                    ":sex" => (isset($_POST['sex']) ? $_POST['sex'] : false),

                    ":address" => (isset($_POST['address']) ? $_POST['address'] : false),

                    ":province" => (isset($_POST['province']) ? $_POST['province'] : false),

                    ":phone" => (isset($_POST['phone']) ? $_POST['phone'] : false),

                    ":mobile" => (isset($_POST['mobile']) ? $_POST['mobile'] : false),

                    ":email" => (isset($_POST['email']) ? $_POST['email'] : false),

                    ":project" => (isset($project) ? $project : false),

                    ":date" => (isset($date) ? $date : false),

                    ":time" => (isset($time) ? $time : false),

                    ":redeemer" => (isset($_POST['redeemer']) ? $_POST['redeemer'] : false),

                    ":redeemer_name" => (isset($_POST['redeemer_name']) ? $_POST['redeemer_name'] : false),

                    ":eq" => (isset($_POST['eq']) ? $_POST['eq'] : 0),

                    ":fix" => (isset($_POST['fix']) ? $_POST['fix'] : 0),

                    ":mix" => (isset($_POST['mix']) ? $_POST['mix'] : 0),

                    ":fif" => (isset($_POST['fif']) ? $_POST['fif'] : 0),

                    ":prop" => (isset($_POST['prop']) ? $_POST['prop'] : 0),

                    ":ltf" => (isset($_POST['ltf']) ? $_POST['ltf'] : 0),

                    ":rmf" => (isset($_POST['rmf']) ? $_POST['rmf'] : 0),

                    ":rmf_eq" => (isset($_POST['rmf_eq']) ? $_POST['rmf_eq'] : 0),

                    ":esg" => (isset($_POST['esg']) ? $_POST['esg'] : 0),

                    ":other" => (isset($_POST['other']) ? $_POST['other'] : 0),

                    ":pro" => (isset($pro) ? $pro : false),

                    ":new_gen" => (isset($new_gen) ? $new_gen : 0),

                    ":asset_name" => (isset($asset_name) ? $asset_name : false),

                    ":howmuch" => (isset($_POST['howmuch']) ? $_POST['howmuch'] : false),

                    ":howmuch_dca" => (isset($_POST['howmuch_dca']) ? $_POST['howmuch_dca'] : false),

                    ":reader" => (isset($_POST['reader']) ? $_POST['reader'] : 0)

                ));

                $pdo->commit();

            }

            catch (Exception $e) {

                $pdo->rollback();

                throw $e;

            }

            if (isset($e)) {

                echo "Cannot add new customer, Please contact Administrator<br/>";

                echo $e->getMessage();

            } else {

                echo "<script type=\"text/javascript\">

                    alert(\"ลงทะเบียนแลกของเรียบร้อยแล้วค่ะ\");

                    window.location=\"add.php\";

                    </script>";

            }

        }

    }

}

?>





<?php

/*

if($_GET[inserted]){

?>

<script type="text/javascript">
alert("ลงทะเบียนแลกของเรียบร้อยแล้วค่ะ");
</script>

<?

}

*/

?>



<html>

<head>

    <title>Redemption Point</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="css/main_style.css" rel="stylesheet" type="text/css">

    <script src="ajax2.js"></script>

    <script src="js/jquery-3.1.0.min.js"></script>

    <script type="text/javascript" src="js/com_script.js"></script>

    <script type="text/javascript">
    var isChecked = false;


    function showAlert(message, color) {
        document.getElementById("alertMessage").textContent = message;
        document.getElementById("alertMessage").style.color = color;
        document.getElementById("customAlert").style.display = "block";
        
    }

    function closeAlert() {
        document.getElementById("alertMessage").textContent = "";
        document.getElementById("alertMessage").style.color = "white";
        document.getElementById("customAlert").style.display = "none";
    }
    // Call showAlert() whenever you want to show the alert

    function check(element) {



        if (element.checked == true) {

            document.getElementById(element.name + "_text_id").style.display = "";

            document.getElementById("txtHint2").style.display = "none";

            document.getElementById("txtHint4").innerHTML = "";

        } else {

            document.getElementById(element.name + "_text_id").style.display = "none";

            checkAll(document.getElementById("idCard").value, "");

        }

    }

    function preCheck(element) {

        checkId2(element);

    }
    </script>

</head>



<body id="mainBody" bgcolor="#FFFFFF" text="#000000">

    <input type="hidden" id="lockBoard" name="lockBoard" value="0">

    <table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

        <tr>

            <td height="1" background="images/point.jpg"></td>

            <td height="1" background="images/point.jpg"></td>

            <td width="4" height="1" background="images/shadowpoint.jpg"></td>

        </tr>

        <tr>

            <td width="1" background="images/point.jpg"></td>

            <td>

                <?php

if (isset($pro)) {

    if ($pro == "cam") {

        $bgcolor = "bgcolor = '#FFFFCC'";

    } else {

        $bgcolor = "";

    }

}

?>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" <?php

(isset($bgcolor) ? $bgcolor : false);

?>>

                    <form name="redemption" id="redemption" action="<?php

echo htmlentities($_SERVER['PHP_SELF']);

?>" method="post">

                        <tr>

                            <td colspan="2">

                                <br>

                                <div align="center">

                                    <font size='5'>

                                        Redemption Point<br>

                                        <?php

try {

    $getProjectQuery = $pdo->query("SELECT * FROM `project` WHERE `project` = '" . $project . "'");

}

catch (PDOExeption $e) {

    die("Query failed: " . $e . getMessage());

}

$projectResult = $getProjectQuery->fetch();

$date          = date("d") . "/" . date("m") . "/" . (date("Y") + 543);



if ($what == "set") {

    //$time_start = microtime(true);

    try {

        $getStockQuery = $pdo->query("SELECT count(*) FROM `set` WHERE `stock` = '1' and `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_stock = (($rs_stock = $getStockQuery->fetchColumn()) > 0 ? $rs_stock : 0);





    // $time_end = microtime(true);

    // $time = $time_end - $time_start;

    // echo "Process Time: {$time}";



    try {

        $getFuturesQuery = $pdo->query("SELECT count(*) FROM `set` WHERE `futures` = '1' and `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_futures = (($rs_futures = $getFuturesQuery->fetchColumn()) > 0 ? $rs_futures : 0);



    try {

        $getMFQuery = $pdo->query("SELECT count(*) FROM `set` WHERE `mf` = '1' and `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_mf = (($rs_mf = $getMFQuery->fetchColumn()) > 0 ? $rs_mf : 0);



    try {

        $getPro1Query = $pdo->query("SELECT count(*) FROM `set` WHERE `pro1` = '1' and `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_pro1 = (($rs_pro1 = $getPro1Query->fetchColumn()) > 0 ? $rs_pro1 : 0);



    try {

        $getSETVoucherQuery = $pdo->query("SELECT count(*) FROM `set` WHERE `set_voucher` = '1' and `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_set_voucher = (($rs_set_voucher = $getSETVoucherQuery->fetchColumn()) > 0 ? $rs_set_voucher : 0);



    try {

        $getTFEXVoucherQuery = $pdo->query("SELECT count(*) FROM `set` WHERE `tfex_voucher` = '1' and `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_tfex_voucher = (($rs_tfex_voucher = $getTFEXVoucherQuery->fetchColumn()) > 0 ? $rs_tfex_voucher : 0);



    try {

        $getDoubleVoucherQuery = $pdo->query("SELECT count(*) FROM `set` WHERE `double_voucher` = '1' and `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_double_voucher = (($rs_double_voucher = $getDoubleVoucherQuery->fetchColumn()) > 0 ? $rs_double_voucher : 0);



    try {

        $getB2BVoucherQuery = $pdo->query("SELECT count(*) FROM `set` WHERE `b2b_voucher` = '1' and `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_b2b_voucher = (($rs_b2b_voucher = $getB2BVoucherQuery->fetchColumn()) > 0 ? $rs_b2b_voucher : 0);





    echo "<p>หุ้น " . $rs_stock . "&nbsp;&nbsp;<font color='0000FF'>อนุพันธ์ " . $rs_futures . "</font>&nbsp;&nbsp;<font color='000000'>กองทุนออนไลน์ " . $rs_mf . "</font></p>";



    echo '

                <img src="images/set.jpg"><br><a href="add.php?pro=' . (isset($pro) ? $pro : false) . '&project=' . $project . '&what=funds">เปลี่ยน</a>';



} else {

    try {

        $getFundQuery = $pdo->query("SELECT count(*) FROM `funds` WHERE `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_fund = (($rs_fund = $getFundQuery->fetchColumn()) > 0 ? $rs_fund : 0);



    try {

        $getFundDCAQuery = $pdo->query("SELECT count(*) FROM `funds` WHERE `howmuch_dca` > 0 and `project` = '" . $projectResult['project'] . "' and `date` = '" . $date . "'");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $rs_countdca = (($rs_countdca = $getFundDCAQuery->fetchColumn()) > 0 ? $rs_countdca : 0);



    echo "<p>กองทุน " . $rs_fund . "</p>";

    echo '

                <img src="images/mf.jpg"><br><a href="add.php?pro=' . $pro . '&project=' . $project . '&what=set">เปลี่ยน</a>';

}

echo "<br>";

if ($what == "funds") {

    if ($pro == "pro") {

        echo "Promotion";

    } else {

        echo "Campaign";

    }

}

?>

                                    </font>

                                </div>

                                <br>

                            </td>

                        </tr>

                        <tr>

                            <td width="65%">

                                <div align="center">งาน

                                    <?= $project ?><br>



                                </div>

                            </td>

                            <td>



                                <div align="center">ปี

                                    <?= $projectResult['year'] ?><br>

                                </div>

                            </td>

                        </tr>

                        <tr>

                            <td>

                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td height="10" align="right" width="27%"></td>

                                        <td height="10" align="center" width="2%"></td>

                                        <td height="10" align="left" width="71%"></td>

                                    </tr>

                                    <tr>

                                        <td align="right" width="27%">ประเภทการตรวจสอบ</td>

                                        <td align="center" width="2%">:</td>

                                        <td align="left" width="71%">
                                            <div  class="inline">
                                                <p id="id_type">idcard</p>
                                            </div>&nbsp;<input type='button' value='เปลี่ยน'
                                                id='toggle_id'><label><input type="checkbox" name="reader" id="reader"
                                                    value="1" style="margin-left:20px;">reader</label>
                                        </td>

                                    </tr>

                                    <tr>

                                        <td height="10" align="right" width="27%"></td>

                                        <td height="10" align="center" width="2%"></td>

                                        <td height="10" align="left" width="71%"></td>

                                    </tr>

                                    <tr>

                                        <td align="right" width="27%">
                                            <div id="id_type2" class="inline">
                                                <p>เลขที่บัตรประชาชน</p>
                                        </td>

                                        <td align="center" width="2%">:</td>

                                        <td align="left" width="71%">

                                            <input type="number" name="id" id="idCard" size="20" maxlength="13"
                                                onkeyup="checkAll(this.value,'<?= (isset($pro)?$pro:false) ?>',$('#id_type').text())"
                                                value="<?php

echo (isset($_POST['id']) ? $_POST['id'] : false);

?>">

                                            <input type="hidden" name="hidden_id" id="hidden_id" value="">

                                            <font color="#FF0000">* </font>
                                        </td>

                                    </tr>

                                    <tr>

                                        <td align="right" width="27%">&nbsp;</td>

                                        <td align="center" width="2%">&nbsp;</td>

                                        <td align="left" width="71%">

                                            <div id="txtHint"><input type="button" value="ตรวจสอบ"
                                                    onclick='checkAll($("#idCard").val(),"<?= (isset($pro)?$pro:false) ?>",$("#id_type").text())'>
                                            </div>

                                        </td>

                                    </tr>

                                    <?php

if ($what == "set") {

    echo '

                    <tr>

                      <td align="right" width="35%">บริษัท</td>

                      <td align="center" width="3%">:</td>

                      <td align="left" width="62%">

                        <select name="sec_name" id="sec_name">

                          <option value=""></option>';

    try {

        $getSecComQuery = $pdo->query("SELECT * FROM `sec_com` WHERE `status` = 'Y' ORDER BY `nick_name` ASC");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $secComResults = $getSecComQuery->fetchAll();



    foreach ($secComResults as $secCom) {

        if ($_POST[sec_name] == $secCom['name']) {

            $select_sec = "selected";

        } else {

            $select_sec = "";

        }

        echo '

                          <option value="' . $secCom['name'] . '" ' . $select_sec . '>

                          ' . $secCom['nick_name'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $secCom['name'] . '

                          </option>';

    }

    echo '

                        </select>

                        <font color="#FF0000">*</font>' . $text . '</td>

                    </tr>';



} else if ($what == "funds") {

    echo '

                    <tr>

                      <td align="right" width="35%">บริษัท</td>

                      <td align="center" width="3%">:</td>

                      <td align="left" width="62%">

                        <select name="asset_name" id="asset_name">

                          <option value=""></option>';

    try {

        $getAssetComQuery = $pdo->query("SELECT * FROM `asset_com` WHERE `status` = 'Y' ORDER BY `nick_name` ASC");

    }

    catch (PDOExeption $e) {

        die("Query failed: " . $e . getMessage());

    }

    $assetComResults = $getAssetComQuery->fetchAll();



    foreach ($assetComResults as $assetCom) {

        if ($_POST[asset_name] == $assetCom['name']) {

            $select_asset = "selected";

        } else {

            $select_asset = "";

        }

        echo '

                          <option value="' . $assetCom['name'] . '" ' . $select_asset . '>

                          ' . $assetCom['nick_name'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $assetCom['name'] . '

                          </option>';

    }

    echo '

                        </select>

                        <font color="#FF0000">*</font>' . $text . '</td>

                    </tr>';

}

?>

                                    <tr>

                                        <td align="right" width="27%">ชื่อ</td>

                                        <td align="center" width="2%">:</td>

                                        <td align="left" width="71%">

                                            <input type="text" name="fname" id="fname" size="20" maxlength="20" value="<?php

echo (isset($_POST['fname']) ? $_POST['fname'] : false);

?>">

                                        </td>

                                    </tr>

                                    <tr>

                                        <td align="right" width="27%">นามสกุล</td>

                                        <td align="center" width="2%">:</td>

                                        <td align="left" width="71%">

                                            <input type="text" name="sname" id="sname" size="20" maxlength="20" value="<?php

echo (isset($_POST['sname']) ? $_POST['sname'] : false);

?>">

                                        </td>

                                    </tr>

                                    <tr>

                                        <td align="right" width="27%" height="23">วัน/เดือน/ปีเกิด</td>

                                        <td align="center" width="2%" height="23">:</td>

                                        <td align="left" width="71%" height="23">

                                            วัน<input type="text" name="dob" id="dob" size="1" maxlength="2" value="<?php

echo (isset($_POST['dob']) ? $_POST['dob'] : false);

?>">

                                            เดือน<input type="text" name="mob" id="mob" size="1" maxlength="2" value="<?php

echo (isset($_POST['mob']) ? $_POST['mob'] : false);

?>">

                                            ปี<input type="text" name="yob" id="yob" size="2" maxlength="4" value="<?php

echo (isset($_POST['yob']) ? $_POST['yob'] : false);

?>"> (ค.ศ. เช่น 1987)

                                        </td>

                                    </tr>

                                    <tr>

                                        <td align="right" width="27%">เพศ</td>

                                        <td align="center" width="2%">:</td>

                                        <td align="left" width="71%">

                                            <?php

if (isset($_POST['sex'])) {

    switch ($_POST['sex']) {

        case "M":

            $sex_check01 = "selected";

            break;

        case "F":

            $sex_check02 = "selected";

            break;

    } //end case

}

?>

                                            <select name="sex" id="sex" style="width:146px;"
                                                onchange="check_sex(this.value);">

                                                <option value="">กรุณาเลือก</option>

                                                <option value="M" <?= (isset($sex_check01) ? $sex_check01 : false); ?>>
                                                    ชาย</option>

                                                <option value="F" <?= (isset($sex_check02) ? $sex_check02 : false); ?>>
                                                    หญิง</option>

                                            </select>

                                            <input type="text" name="sexText" id="sexText" value="<?php

echo (isset($_POST['sex']) ? $_POST['sex'] : false);

?>" style="width:15px;color:white;" onblur="check_sex(this.value);">

                                        </td>

                                    </tr>

                                    <?php

if ($what == "funds") {

?>

                                    <tr>

                                        <td align="right" width="27%">&nbsp;</td>

                                        <td align="center" width="2%">&nbsp;</td>

                                        <td align="left" width="71%">

                                            <div id="txtHint3"></div>

                                        </td>

                                    </tr>

                                    <?php

}

?>

                                    <tr>

                                        <td align="right" width="27%">ที่อยู่</td>

                                        <td align="center" width="2%">:</td>

                                        <td align="left" width="71%">

                                            <input type="text" name="address" id="address" size="40" maxlength="255"
                                                value="<?php

echo (isset($_POST['address']) ? $_POST['address'] : false);

?>">

                                        </td>

                                    </tr>

                                    <tr>

                                        <td align="right" width="27%">จังหวัด</td>

                                        <td align="center" width="2%">:</td>

                                        <td align="left" width="71%">

                                            <?php

if (isset($_POST['province'])) {

    switch ($_POST['province']) {

        case "กรุงเทพ":

            $province_check01 = "selected";

            break;

        case "กระบี่":

            $province_check02 = "selected";

            break;

        case "กาญจนบุรี":

            $province_check03 = "selected";

            break;

        case "กาฬสินธุ์":

            $province_check04 = "selected";

            break;

        case "กำแพงเพชร":

            $province_check05 = "selected";

            break;

        case "ขอนแก่น":

            $province_check06 = "selected";

            break;

        case "จันทบุรี":

            $province_check07 = "selected";

            break;

        case "ฉะเชิงเทรา":

            $province_check08 = "selected";

            break;

        case "ชลบุรี":

            $province_check09 = "selected";

            break;

        case "ชัยนาท":

            $province_check10 = "selected";

            break;

        case "ชัยภูมิ":

            $province_check11 = "selected";

            break;

        case "ชุมพร":

            $province_check12 = "selected";

            break;

        case "เชียงราย":

            $province_check13 = "selected";

            break;

        case "เชียงใหม่":

            $province_check14 = "selected";

            break;

        case "ตรัง":

            $province_check15 = "selected";

            break;

        case "ตราด":

            $province_check16 = "selected";

            break;

        case "ตาก":

            $province_check17 = "selected";

            break;

        case "นครนายก":

            $province_check18 = "selected";

            break;

        case "นครปฐม":

            $province_check19 = "selected";

            break;

        case "นครพนม":

            $province_check20 = "selected";

            break;

        case "นครราชสีมา":

            $province_check21 = "selected";

            break;

        case "นครศรีธรรมราช":

            $province_check22 = "selected";

            break;

        case "นครสวรรค์":

            $province_check23 = "selected";

            break;

        case "นนทบุรี":

            $province_check24 = "selected";

            break;

        case "นราธิวาส":

            $province_check25 = "selected";

            break;

        case "น่าน":

            $province_check26 = "selected";

            break;

        case "หนองคาย":

            $province_check27 = "selected";

            break;

        case "หนองบัวลำภู":

            $province_check28 = "selected";

            break;

        case "บึงกาฬ":

            $province_check77 = "selected";

            break;

        case "บุรีรัมย์":

            $province_check29 = "selected";

            break;

        case "ปทุมธานี":

            $province_check30 = "selected";

            break;

        case "ประจวบคีรีขันธ์":

            $province_check31 = "selected";

            break;

        case "ปราจีนบุรี":

            $province_check32 = "selected";

            break;

        case "ปัตตานี":

            $province_check33 = "selected";

            break;

        case "พระนครศรีอยุธยา":

            $province_check34 = "selected";

            break;

        case "พังงา":

            $province_check35 = "selected";

            break;

        case "พัทลุง":

            $province_check36 = "selected";

            break;

        case "พิจิตร":

            $province_check37 = "selected";

            break;

        case "พิษณุโลก":

            $province_check38 = "selected";

            break;

        case "เพชรบุรี":

            $province_check39 = "selected";

            break;

        case "เพชรบูรณ์":

            $province_check40 = "selected";

            break;

        case "แพร่":

            $province_check41 = "selected";

            break;

        case "พะเยา":

            $province_check42 = "selected";

            break;

        case "ภูเก็ต":

            $province_check43 = "selected";

            break;

        case "มหาสารคาม":

            $province_check44 = "selected";

            break;

        case "แม่ฮ่องสอน":

            $province_check45 = "selected";

            break;

        case "มุกดาหาร":

            $province_check46 = "selected";

            break;

        case "ยะลา":

            $province_check47 = "selected";

            break;

        case "ยโสธร":

            $province_check48 = "selected";

            break;

        case "ร้อยเอ็ด":

            $province_check49 = "selected";

            break;

        case "ระนอง":

            $province_check50 = "selected";

            break;

        case "ระยอง":

            $province_check51 = "selected";

            break;

        case "ราชบุรี":

            $province_check52 = "selected";

            break;

        case "ลพบุรี":

            $province_check53 = "selected";

            break;

        case "ลำปาง":

            $province_check54 = "selected";

            break;

        case "ลำพูน":

            $province_check55 = "selected";

            break;

        case "เลย":

            $province_check56 = "selected";

            break;

        case "ศรีสะเกษ":

            $province_check57 = "selected";

            break;

        case "สกลนคร":

            $province_check58 = "selected";

            break;

        case "สงขลา":

            $province_check59 = "selected";

            break;

        case "สตูล":

            $province_check60 = "selected";

            break;

        case "สมุทรปราการ":

            $province_check61 = "selected";

            break;

        case "สมุทรสงคราม":

            $province_check62 = "selected";

            break;

        case "สมุทรสาคร":

            $province_check63 = "selected";

            break;

        case "สระแก้ว":

            $province_check64 = "selected";

            break;

        case "สระบุรี":

            $province_check65 = "selected";

            break;

        case "สิงห์บุรี":

            $province_check66 = "selected";

            break;

        case "สุโขทัย":

            $province_check67 = "selected";

            break;

        case "สุพรรณบุรี":

            $province_check68 = "selected";

            break;

        case "สุราษฎร์ธานี":

            $province_check69 = "selected";

            break;

        case "สุรินทร์":

            $province_check70 = "selected";

            break;

        case "อ่างทอง":

            $province_check71 = "selected";

            break;

        case "อุดรธานี":

            $province_check72 = "selected";

            break;

        case "อุทัยธานี":

            $province_check73 = "selected";

            break;

        case "อุตรดิตถ์":

            $province_check74 = "selected";

            break;

        case "อุบลราชธานี":

            $province_check75 = "selected";

            break;

        case "อำนาจเจริญ":

            $province_check76 = "selected";

            break;

    } //end case

}

?>

                                            <select name="province" id="province" style="width:146px"
                                                onchange="check_province(this.value);">

                                                <option value="">กรุณาเลือก</option>

                                                <option value="กรุงเทพ"
                                                    <?= (isset($province_check01) ? $province_check01 : false); ?>>
                                                    กรุงเทพ</option>

                                                <option value="กระบี่"
                                                    <?= (isset($province_check02) ? $province_check02 : false); ?>>
                                                    กระบี่</option>

                                                <option value="กาญจนบุรี"
                                                    <?= (isset($province_check03) ? $province_check03 : false); ?>>
                                                    กาญจนบุรี</option>

                                                <option value="กาฬสินธุ์"
                                                    <?= (isset($province_check04) ? $province_check04 : false); ?>>
                                                    กาฬสินธุ์</option>

                                                <option value="กำแพงเพชร"
                                                    <?= (isset($province_check05) ? $province_check05 : false); ?>>
                                                    กำแพงเพชร</option>

                                                <option value="ขอนแก่น"
                                                    <?= (isset($province_check06) ? $province_check06 : false); ?>>
                                                    ขอนแก่น</option>

                                                <option value="จันทบุรี"
                                                    <?= (isset($province_check07) ? $province_check07 : false); ?>>
                                                    จันทบุรี</option>

                                                <option value="ฉะเชิงเทรา"
                                                    <?= (isset($province_check08) ? $province_check08 : false); ?>>
                                                    ฉะเชิงเทรา</option>

                                                <option value="ชลบุรี"
                                                    <?= (isset($province_check09) ? $province_check09 : false); ?>>
                                                    ชลบุรี</option>

                                                <option value="ชัยนาท"
                                                    <?= (isset($province_check10) ? $province_check10 : false); ?>>
                                                    ชัยนาท</option>

                                                <option value="ชัยภูมิ"
                                                    <?= (isset($province_check11) ? $province_check11 : false); ?>>
                                                    ชัยภูมิ</option>

                                                <option value="ชุมพร"
                                                    <?= (isset($province_check12) ? $province_check12 : false); ?>>ชุมพร
                                                </option>

                                                <option value="เชียงราย"
                                                    <?= (isset($province_check13) ? $province_check13 : false); ?>>
                                                    เชียงราย</option>

                                                <option value="เชียงใหม่"
                                                    <?= (isset($province_check14) ? $province_check14 : false); ?>>
                                                    เชียงใหม่</option>

                                                <option value="ตรัง"
                                                    <?= (isset($province_check15) ? $province_check15 : false); ?>>ตรัง
                                                </option>

                                                <option value="ตราด"
                                                    <?= (isset($province_check16) ? $province_check16 : false); ?>>ตราด
                                                </option>

                                                <option value="ตาก"
                                                    <?= (isset($province_check17) ? $province_check17 : false); ?>>ตาก
                                                </option>

                                                <option value="นครนายก"
                                                    <?= (isset($province_check18) ? $province_check18 : false); ?>>
                                                    นครนายก</option>

                                                <option value="นครปฐม"
                                                    <?= (isset($province_check19) ? $province_check19 : false); ?>>
                                                    นครปฐม</option>

                                                <option value="นครพนม"
                                                    <?= (isset($province_check20) ? $province_check20 : false); ?>>
                                                    นครพนม</option>

                                                <option value="นครราชสีมา"
                                                    <?= (isset($province_check21) ? $province_check21 : false); ?>>
                                                    นครราชสีมา</option>

                                                <option value="นครศรีธรรมราช"
                                                    <?= (isset($province_check22) ? $province_check22 : false); ?>>
                                                    นครศรีธรรมราช</option>

                                                <option value="นครสวรรค์"
                                                    <?= (isset($province_check23) ? $province_check23 : false); ?>>
                                                    นครสวรรค์</option>

                                                <option value="นนทบุรี"
                                                    <?= (isset($province_check24) ? $province_check24 : false); ?>>
                                                    นนทบุรี</option>

                                                <option value="นราธิวาส"
                                                    <?= (isset($province_check25) ? $province_check25 : false); ?>>
                                                    นราธิวาส</option>

                                                <option value="น่าน"
                                                    <?= (isset($province_check26) ? $province_check26 : false); ?>>น่าน
                                                </option>

                                                <option value="หนองคาย"
                                                    <?= (isset($province_check27) ? $province_check27 : false); ?>>
                                                    หนองคาย</option>

                                                <option value="หนองบัวลำภู"
                                                    <?= (isset($province_check28) ? $province_check28 : false); ?>>
                                                    หนองบัวลำภู</option>

                                                <option value="บึงกาฬ"
                                                    <?= (isset($province_check77) ? $province_check77 : false); ?>>
                                                    บึงกาฬ</option>

                                                <option value="บุรีรัมย์"
                                                    <?= (isset($province_check29) ? $province_check29 : false); ?>>
                                                    บุรีรัมย์</option>

                                                <option value="ปทุมธานี"
                                                    <?= (isset($province_check30) ? $province_check30 : false); ?>>
                                                    ปทุมธานี</option>

                                                <option value="ประจวบคีรีขันธ์"
                                                    <?= (isset($province_check31) ? $province_check31 : false); ?>>
                                                    ประจวบคีรีขันธ์</option>

                                                <option value="ปราจีนบุรี"
                                                    <?= (isset($province_check32) ? $province_check32 : false); ?>>
                                                    ปราจีนบุรี</option>

                                                <option value="ปัตตานี"
                                                    <?= (isset($province_check33) ? $province_check33 : false); ?>>
                                                    ปัตตานี</option>

                                                <option value="พระนครศรีอยุธยา"
                                                    <?= (isset($province_check34) ? $province_check34 : false); ?>>
                                                    พระนครศรีอยุธยา</option>

                                                <option value="พังงา"
                                                    <?= (isset($province_check35) ? $province_check35 : false); ?>>พังงา
                                                </option>

                                                <option value="พัทลุง"
                                                    <?= (isset($province_check36) ? $province_check36 : false); ?>>
                                                    พัทลุง</option>

                                                <option value="พิจิตร"
                                                    <?= (isset($province_check37) ? $province_check37 : false); ?>>
                                                    พิจิตร</option>

                                                <option value="พิษณุโลก"
                                                    <?= (isset($province_check38) ? $province_check38 : false); ?>>
                                                    พิษณุโลก</option>

                                                <option value="เพชรบุรี"
                                                    <?= (isset($province_check39) ? $province_check39 : false); ?>>
                                                    เพชรบุรี</option>

                                                <option value="เพชรบูรณ์"
                                                    <?= (isset($province_check40) ? $province_check40 : false); ?>>
                                                    เพชรบูรณ์</option>

                                                <option value="แพร่"
                                                    <?= (isset($province_check41) ? $province_check41 : false); ?>>แพร่
                                                </option>

                                                <option value="พะเยา"
                                                    <?= (isset($province_check42) ? $province_check42 : false); ?>>พะเยา
                                                </option>

                                                <option value="ภูเก็ต"
                                                    <?= (isset($province_check43) ? $province_check43 : false); ?>>
                                                    ภูเก็ต</option>

                                                <option value="มหาสารคาม"
                                                    <?= (isset($province_check44) ? $province_check44 : false); ?>>
                                                    มหาสารคาม</option>

                                                <option value="แม่ฮ่องสอน"
                                                    <?= (isset($province_check45) ? $province_check45 : false); ?>>
                                                    แม่ฮ่องสอน</option>

                                                <option value="มุกดาหาร"
                                                    <?= (isset($province_check46) ? $province_check46 : false); ?>>
                                                    มุกดาหาร</option>

                                                <option value="ยะลา"
                                                    <?= (isset($province_check47) ? $province_check47 : false); ?>>ยะลา
                                                </option>

                                                <option value="ยโสธร"
                                                    <?= (isset($province_check48) ? $province_check48 : false); ?>>ยโสธร
                                                </option>

                                                <option value="ร้อยเอ็ด"
                                                    <?= (isset($province_check49) ? $province_check49 : false); ?>>
                                                    ร้อยเอ็ด</option>

                                                <option value="ระนอง"
                                                    <?= (isset($province_check50) ? $province_check50 : false); ?>>ระนอง
                                                </option>

                                                <option value="ระยอง"
                                                    <?= (isset($province_check51) ? $province_check51 : false); ?>>ระยอง
                                                </option>

                                                <option value="ราชบุรี"
                                                    <?= (isset($province_check52) ? $province_check52 : false); ?>>
                                                    ราชบุรี</option>

                                                <option value="ลพบุรี"
                                                    <?= (isset($province_check53) ? $province_check53 : false); ?>>
                                                    ลพบุรี</option>

                                                <option value="ลำปาง"
                                                    <?= (isset($province_check54) ? $province_check54 : false); ?>>ลำปาง
                                                </option>

                                                <option value="ลำพูน"
                                                    <?= (isset($province_check55) ? $province_check55 : false); ?>>ลำพูน
                                                </option>

                                                <option value="เลย"
                                                    <?= (isset($province_check56) ? $province_check56 : false); ?>>เลย
                                                </option>

                                                <option value="ศรีสะเกษ"
                                                    <?= (isset($province_check57) ? $province_check57 : false); ?>>
                                                    ศรีสะเกษ</option>

                                                <option value="สกลนคร"
                                                    <?= (isset($province_check58) ? $province_check58 : false); ?>>
                                                    สกลนคร</option>

                                                <option value="สงขลา"
                                                    <?= (isset($province_check59) ? $province_check59 : false); ?>>สงขลา
                                                </option>

                                                <option value="สตูล"
                                                    <?= (isset($province_check60) ? $province_check60 : false); ?>>สตูล
                                                </option>

                                                <option value="สมุทรปราการ"
                                                    <?= (isset($province_check61) ? $province_check61 : false); ?>>
                                                    สมุทรปราการ</option>

                                                <option value="สมุทรสงคราม"
                                                    <?= (isset($province_check62) ? $province_check62 : false); ?>>
                                                    สมุทรสงคราม</option>

                                                <option value="สมุทรสาคร"
                                                    <?= (isset($province_check63) ? $province_check63 : false); ?>>
                                                    สมุทรสาคร</option>

                                                <option value="สระแก้ว"
                                                    <?= (isset($province_check64) ? $province_check64 : false); ?>>
                                                    สระแก้ว</option>

                                                <option value="สระบุรี"
                                                    <?= (isset($province_check65) ? $province_check65 : false); ?>>
                                                    สระบุรี</option>

                                                <option value="สิงห์บุรี"
                                                    <?= (isset($province_check66) ? $province_check66 : false); ?>>
                                                    สิงห์บุรี</option>

                                                <option value="สุโขทัย"
                                                    <?= (isset($province_check67) ? $province_check67 : false); ?>>
                                                    สุโขทัย</option>

                                                <option value="สุพรรณบุรี"
                                                    <?= (isset($province_check68) ? $province_check68 : false); ?>>
                                                    สุพรรณบุรี</option>

                                                <option value="สุราษฎร์ธานี"
                                                    <?= (isset($province_check69) ? $province_check69 : false); ?>>
                                                    สุราษฎร์ธานี</option>

                                                <option value="สุรินทร์"
                                                    <?= (isset($province_check70) ? $province_check70 : false); ?>>
                                                    สุรินทร์</option>

                                                <option value="อ่างทอง"
                                                    <?= (isset($province_check71) ? $province_check71 : false); ?>>
                                                    อ่างทอง</option>

                                                <option value="อุดรธานี"
                                                    <?= (isset($province_check72) ? $province_check72 : false); ?>>
                                                    อุดรธานี</option>

                                                <option value="อุทัยธานี"
                                                    <?= (isset($province_check73) ? $province_check73 : false); ?>>
                                                    อุทัยธานี</option>

                                                <option value="อุตรดิตถ์"
                                                    <?= (isset($province_check74) ? $province_check74 : false); ?>>
                                                    อุตรดิตถ์</option>

                                                <option value="อุบลราชธานี"
                                                    <?= (isset($province_check75) ? $province_check75 : false); ?>>
                                                    อุบลราชธานี</option>

                                                <option value="อำนาจเจริญ"
                                                    <?= (isset($province_check76) ? $province_check76 : false); ?>>
                                                    อำนาจเจริญ</option>

                                            </select>

                                            <input type="text" name="provinceText" id="provinceText" value="<?php

echo (isset($_POST['province']) ? $_POST['province'] : false);

?>" style="width:15px;color:white;" onblur="check_province(this.value);">

                                        </td>

                                    </tr>

                                    <!--<tr>

                  <td align="right" width="27%">เบอร์โทร</td>

                  <td align="center" width="2%">:</td>

                  <td align="left" width="71%">

                    <input type="text" name="phone" maxlength="15" size="20" value="<?php

echo (isset($_POST['phone']) ? $_POST['phone'] : false);

?>">

                  </td>

                </tr>-->

                                    <tr>

                                        <td align="right" width="27%">มือถือ</td>

                                        <td align="center" width="2%">:</td>

                                        <td align="left" width="71%">

                                            <input type="number" name="mobile" id="mobile" maxlength="10" size="20"
                                                value="<?php

echo (isset($_POST['mobile']) ? $_POST['mobile'] : false);

?>">

                                            <font color="red">*</font>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td align="right" width="27%">E-mail</td>

                                        <td align="center" width="2%">:</td>

                                        <td align="left" width="71%">

                                            <input type="email" name="email" id="email" size="20" value="<?php

echo (isset($_POST['email']) ? $_POST['email'] : false);

?>">

                                            <font color="red">*</font>

                                        </td>

                                    </tr>

                        </tr>

                        <tr>

                            <td align="right" width="27%">ผู้รับ</td>

                            <td align="center" width="2%">:</td>

                            <td align="left" width="71%">

                                <select name="redeemer" id="redeemer">

                                    <?php

if (isset($_POST['redeemer'])) {

    if ($_POST[redeemer] == "customer") {

        $select_customer = "selected";

    } else if ($_POST[redeemer] == "marketing") {

        $select_marketing = "selected";

    } else {

        $select_no_cus_mar = "selected";

        $_POST['redeemer_name'] = "-";

    }

}

echo <<<REDEEMER

<option value="" $select_no_cus_mar>โปรดระบุ</option>

<option value="customer" $select_customer>ลูกค้า</option>

<option value="marketing" $select_marketing >พนักงาน</option>



REDEEMER;

?>



                                </select>
                                <font color="red">*</font>

                            </td>

                        </tr>

                        <tr>

                            <td align="right" width="27%">ชื่อ-ผู้รับ</td>

                            <td align="center" width="2%">:</td>

                            <td align="left" width="71%">

                                <input type="text" name="redeemer_name" id="redeemer_name" size="20" value="<?php

echo (isset($_POST['redeemer_name']) ? $_POST['redeemer_name'] : false);

?>">
                                <font color="red">*</font>

                            </td>

                        </tr>

                        <tr>

                            <td align="right" width="27%"></td>

                            <td align="center" width="2%"></td>

                            <td align="left" width="71%">

                                <div id="showRedeemerText">&nbsp;</div>

                            </td>

                        </tr>

                </table>

            </td>

            <td valign="top" id="investment_type">

                <?php

if ($what == "set") {

    echo '     <table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td>&nbsp;</td>

                <td>

                  <div align="left">&nbsp;<b>เปิดบัญชี</b></div>

                </td>

              </tr>

              <tr>

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="checkbox" name="stock" id="stock" value="1">&nbsp;หุ้น</label></td>

              </tr>

              <tr>

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="checkbox" name="futures" id="futures" value="1">&nbsp;อนุพันธ์</label></td>

              </tr>

              <tr>

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="checkbox" name="mf" value="1">&nbsp;กองทุนออนไลน์</label>

                  </td>

              </tr>

              <tr>

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label>มูลค่าลงทุน<input type="text" name="mf_volume" id="mf_volume" style="width:150px;border:1px dashed red;"></label>

                  </td>

              </tr>

              <tr style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="checkbox" name="checkin" value="1">&nbsp;Check-In</label></td>

              </tr>

              <tr style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="checkbox" name="click2win" value="1">&nbsp;Click2Win</label></td>

              </tr>

              <tr><td colspan=2><hr></td></tr>

              <tr>

                <td>&nbsp;</td>

                <td>

                  <div align="left">&nbsp;<b>รับโปรโมชั่นพิเศษ</b></div>

                </td>

              </tr>

              <tr >

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="checkbox" name="pro1" value="1">&nbsp;ลงทุนสม่ำเสมอ (' . (isset($rs_pro1) ? $rs_pro1 : 0) . ')</label></td>

              </tr>

              <tr style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="checkbox" name="pro2" value="1">&nbsp;Reserve Pro2 (' . (isset($rs_pro2) ? $rs_pro2 : 0) . ')</label></td>

              </tr>

              <tr id="set_voucher_label_area" style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input id="set_voucher_label" type="checkbox" name="set_voucher" value="1">&nbsp;SET Voucher (' . (isset($rs_set_voucher) ? $rs_set_voucher : 0) . ')</label></td>

              </tr>

              <tr id="set_voucher_area" style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                    <select name="set_voucher_id_prefix" id="set_voucher_id_prefix">

                    </select>

                    <input type="text" name="set_voucher_id_subfix" id="set_voucher_id_subfix" style="width:30%;border:1px dashed red;">

                    <img src="images/refresh-icon.png" id="refresh_set_voucher">

                </td>

              </tr>

              <tr id="tfex_voucher_label_area" style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input id="tfex_voucher_label" type="checkbox" name="tfex_voucher" value="1">&nbsp;TFEX Voucher (' . (isset($rs_tfex_voucher) ? $rs_tfex_voucher : 0) . ')</label></td>

              </tr>

              <tr id="tfex_voucher_area" style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                    <select name="tfex_voucher_id_prefix" id="tfex_voucher_id_prefix">

                    </select>

                    <input type="text" name="tfex_voucher_id_subfix" id="tfex_voucher_id_subfix" style="width:30%;border:1px dashed red;">

                    <img src="images/refresh-icon.png" id="refresh_tfex_voucher">

                </td>

                </td>

              </tr>

              <tr id="double_voucher_label_area" style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input id="double_voucher_label" type="checkbox" name="double_voucher" value="1">&nbsp;Voucher เปิดบัญชีคู่ (' . (isset($rs_double_voucher) ? $rs_double_voucher : 0) . ')</label></td>

              </tr>

              <tr id="double_voucher_area" style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                    <select name="double_voucher_id_prefix" id="double_voucher_id_prefix">

                    </select>

                    <input type="text" name="double_voucher_id_subfix" id="double_voucher_id_subfix" style="width:30%;border:1px dashed red;">

                    <img src="images/refresh-icon.png" id="refresh_double_voucher">

                </td>

                </td>

              </tr>

              <tr id="b2b_voucher_label_area" style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input id="b2b_voucher_label" type="checkbox" name="b2b_voucher" value="1">&nbsp;Voucher AomWise (' . (isset($rs_b2b_voucher) ? $rs_b2b_voucher : 0) . ')</label></td>

              </tr>

              <tr id="b2b_voucher_area" style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                    <select name="b2b_voucher_id_prefix" id="b2b_voucher_id_prefix">

                    </select>

                    <input type="text" name="b2b_voucher_id_subfix" id="b2b_voucher_id_subfix" style="width:30%;border:1px dashed red;">

                    <img src="images/refresh-icon.png" id="refresh_b2b_voucher">

                </td>

                </td>

              </tr>

              <tr style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="checkbox" name="pro1" value="1" onclick="check(this)">&nbsp;รับกระเป๋า My 1st Stock</label>

                  </td>

              </tr>

              <tr style="display:none;" id="pro1_text_id">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="text" name="pro1_text" style="width:150px;"></label>

                  </td>

              </tr>

              <tr style="display:none;">

                <td width="10%">&nbsp;</td>

                <td width="90%">

                  <label><input type="checkbox" name="pro2" value="1" onclick="check(this)">&nbsp;รับ TFEX Gift Set</label>

                  </td>

              </tr>



            </table>';

} else if ($what == "funds") {
//กองทุนรวม หุ้น
//กองทุนรวม LTF
    echo '

            <table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">ลงทุนใน</td>
              </tr>

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="eq" value="1">&nbsp;<font color="blue">กองทุนรวม Thai EQ</font></label></td>
              </tr>

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="fix" value="1">&nbsp;<font color="blue">กองทุนรวม Fix</font></label></td>
              </tr>

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="mix" value="1">&nbsp;<font color="blue">กองทุนรวม Mix</font></label></td>
              </tr>

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="fif" value="1">&nbsp;<font color="blue">กองทุนรวม FIF</font></label></td>
              </tr>

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="rmf" value="1">&nbsp;<font color="blue">กองทุนรวม RMF</font></label></td>
              </tr>

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="esg" value="1">&nbsp;<font color="blue">กองทุนรวม Thai ESG</font></label></td>
              </tr>

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="ltf" value="1">&nbsp;<font color="blue">กองทุนรวม SSF</font></label></td>
              </tr>

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="other" value="1">&nbsp;<font color="blue">กองทุนรวม อื่น ๆ</font></label></td>
              </tr>

              <!--<tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="prop" value="1">&nbsp;<font color="blue">กองทุนรวม Prop.</font></label></td>
              </tr>

              <tr>
                <td width="10%">&nbsp;</td>
                <td width="90%">
                <label><input type="checkbox" name="rmf_eq" value="1">&nbsp;<font color="blue">กองทุนรวม RMF หุ้นไทย</font></label></td>
              </tr>-->

              <tr>

                <td width="10%">&nbsp;</td>

                <td width="90%"><label for="howmuch" style="color:red;">ยอดจองซื้อหน่วยลงทุน&nbsp;</label><br><input type="text" name="howmuch" id="howmuch" style="width:200px;">

                </td>

              </tr>

              <tr>

                <td width="10%">&nbsp;</td>

                <td width="90%"><label for="howmuch_dca" style="color:green;">DCA&nbsp;(' . (isset($rs_countdca) ? $rs_countdca : 0) . ')</label><br><input type="text" name="howmuch_dca" id="howmuch_dca" style="width:200px;">

                </td>

              </tr>

              <tr>

                <td width="10%">&nbsp;</td>

                <td width="90%">

                &nbsp;</td>

              </tr>

            </table>';

}

?>





            </td>

        </tr>

        <tr>

            <td colspan="2"> <br>



                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr bgcolor="#FFCCFF">

                        <td width="10%"></td>

                        <td>

                            <input type="hidden" name="add" value="1">

                            <div align="left" id="txtHint2" style="display:none;">

                            </div>

                            <p><input type="button" id="submit_redemption" name="add" value="บันทึกเพิ่ม"
                                    style="display:none;margin:auto;"></div>
                            </p>

                        </td>

                        <td width="10%"> </td>

                    </tr>

                </table>

            </td>

        </tr>

        </form>

    </table>

    </td>

    <td width="4" background="images/shadowpoint.jpg"></td>

    </tr>

    <tr>

        <td height="1" background="images/point.jpg"></td>

        <td height="1" background="images/point.jpg"></td>

        <td width="4" height="1" background="images/shadowpoint.jpg"></td>

    </tr>

    </table>

    <table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

        <tr>

            <form name="form1" method="post" action="<?php

echo htmlentities($_SERVER['PHP_SELF']);

?>">

                <td width="153">

                    <input type="submit" name="home" value="home">

                    <input type="submit" name="back" value="back">

                    <input type="submit" name="new" value="new">

                </td>

                <td width="581">&nbsp;</td>



                <td width="66">

                    <div align="right">

                        <input type="submit" name="report" value="report">

                    </div>

                </td>

            </form>

        </tr>

        <tr>

            <td colspan="3"> </td>

        </tr>

    </table>

    <!-- HTML -->
    <div id="customAlert" style="display: none;" class="alert">
    <p id="alertMessage"></p>
    <button onclick="closeAlert()">OK</button>
    </div>

</body>


</html>

<?php



/*function count_comment($value){

$date = date(d)."/".date(m)."/".(date(Y)+543);

$time = date(H).":".date(i).":".date(s);

$sql_count = "select * from `set` where project = '".$project."' and pro2_text= '".$value."'";

$query_count = mysql_query($sql_count);

$num_row_count = mysql_num_rows($query_count);

return $num_row_count;



<!--<tr style="display:none;" id="pro2_text_id">

<td width="10%">&nbsp;</td>

<td width="90%">

<select name="pro2_text" style="width:150px;border:1px dashed red;">

<option value="Exclusive Seat">Exclusive Seat ('.count_comment('Exclusive Seat').')</option>

<option value="Money Channel">Money Channel ('.count_comment('Money Channel').')</option>

<option value="Stock2Morrow">Stock2Morrow ('.count_comment('Stock2Morrow').')</option>

<option value="TIF">TIF ('.count_comment('TIF').')</option>

<option value="TFEX Station">TFEX Station ('.count_comment('TFEX Station').')</option>

<option value="TFEX Challenge">TFEX Challenge ('.count_comment('TFEX Challenge').')</option>

<option value="ตัดมุมโฆษณา">ตัดมุมโฆษณา ('.count_comment('ตัดมุมโฆษณา').')</option>

<option value="Your1stStock">Your1stStock ('.count_comment('Your1stStock').')</option>

</select>

</td>

</tr>-->

}*/

?>