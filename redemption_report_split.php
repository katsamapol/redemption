<?php

set_time_limit(3000);

require_once("connect.inc.php");
require_once("auth.inc.php");

if (isset($_POST['back'])) {
    header("Location: main_report.php");

}

$what = get_session_or_cookie('what');
$pro = get_session_or_cookie('pro');
$project = get_session_or_cookie('project');

if ($what  == "") {
  header("Location: index.php");
}

if ($project == "") {
  header("Location: main.php");
}

if ($what == "funds") {
  if ($pro == "") {
      header("Location: choose.php");
  }
}

?>

<!doctype HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<title>Untitled Document</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css" href="css/font_style.css">

</head>



<body bgcolor="#FFFFFF" text="#000000">

<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td width="1" background="images/point.jpg"></td>

    <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td colspan="2"> <br>

            <div align="center">Report<br>

              <?php

if ($what == "set") {
    echo '

				<img src="images/set.jpg">';

} else if ($what == "funds") {
    echo '

				<img src="images/mf.jpg">';

}

echo "<br>";

if ($what == "funds") {
    if ($pro == "pro") {
        echo "Promotion";

    } else {
        echo "Campaign";

    }

}

?>

            </div>

          </td>

        </tr>

        <tr>

          <td colspan="2">



            <table width="100%" border="0" cellspacing="1" cellpadding="0">

		<?php

if ($what == "set") {
    $cust    = 0;
    $stock   = 0;
    $futures = 0;
    $round   = 0;
    $sql_d   = "SELECT DISTINCT `date` FROM `set` where `project` = '" . $project . "'";
    try {
        $getDateQuery = $pdo->query($sql_d);
    }
    catch (PDOExeption $e) {
        die("Query failed: " . $e . getMessage());
    }
    $dateResults = $getDateQuery->fetchAll();

    foreach ($dateResults as $rs) { //วันทั้งหมดที่มีงาน แสดงทีละวัน
        $old_cust    = 0;
        $old_stock   = 0;
        $old_futures = 0;
        $new_cust    = 0;
        $new_stock   = 0;
        $new_futures = 0;

        $sql_afd = "select * from `set` WHERE `project` = '" . $project . "' and `date` = '" . $rs['date'] . "'";
        try {
            $getAllFromDateQuery = $pdo->query($sql_afd);
        }
        catch (PDOExeption $e) {
            die("Query failed: " . $e . getMessage());
        }
        $allFromDateResults = $getAllFromDateQuery->fetchAll();

        foreach ($allFromDateResults as $oneFromDateResult) { //เพิ่มค่าทีละคน

            $sql_c_date = "select * from `set` where `id` = '" . $oneFromDateResult['id'] . "' and `project` != '" . $project . "' and `date` != '" . $rs['date'] . "' limit 1";

            try {
                $getCompareToDateQuery = $pdo->query($sql_c_date);
            }
            catch (PDOExeption $e) {
                die("Query failed: " . $e . getMessage());
            }
            $rs2      = $getCompareToDateQuery->fetchAll();
            $num_row3 = count($rs2);

            if ($num_row3 != 0) {
                $old_cust++;

                if (isset($rs2['stock'])) {
                    if ($rs2['stock'] == "1") {
                        $old_stock++;

                    }
                }

                if (isset($rs2['futures'])) {
                    if ($rs2['futures'] == "1") {
                        $old_futures++;

                    }
                }

            } else {
                $new_cust++;

                if (isset($oneFromDateResult['stock'])) {
                    if ($oneFromDateResult['stock'] == "1") {
                        $new_stock++;

                    }
                }
                if (isset($oneFromDateResult['futures'])) {
                    if ($oneFromDateResult['futures'] == "1") {
                        $new_futures++;

                    }
                }

            }

        }

        $old_cust_arr[$round] = $old_cust;

        $old_stock_arr[$round] = $old_stock;

        $old_futures_arr[$round] = $old_futures;

        $new_cust_arr[$round] = $new_cust;

        $new_stock_arr[$round] = $new_stock;

        $new_futures_arr[$round] = $new_futures;

        $date[$round] = $rs['date'];

        $round++;

    }
    $sum_old_cust = 0; $sum_new_cust = 0; $sum_new_stock = 0; $sum_old_stock = 0; $sum_new_futures = 0; $sum_old_futures = 0;
    $sum_old_set = 0; $sum_new_set = 0;
    for ($i = 0; $i < $round; $i++) {
        $sum_old_cust += $old_cust_arr[$i];

        $sum_new_cust += $new_cust_arr[$i];

        $sum_old_stock += $old_stock_arr[$i];

        $sum_new_stock += $new_stock_arr[$i];

        $sum_old_futures += $old_futures_arr[$i];

        $sum_new_futures += $new_futures_arr[$i];



        $old_set_sum[$i] = $old_stock_arr[$i] + $old_futures_arr[$i];

        $new_set_sum[$i] = $new_stock_arr[$i] + $new_futures_arr[$i];

        $sum_old_set += $old_set_sum[$i];

        $sum_new_set += $new_set_sum[$i];

        $cust = $sum_old_cust + $sum_new_cust;

        $stock = $sum_old_stock + $sum_new_stock;

        $futures = $sum_old_futures + $sum_new_futures;

    }

    $sum_sum_set = $sum_old_set + $sum_new_set;

    echo '

				<tr bgcolor="#CCFFFF">

                <td width="9%">

                  <div align="center"></div>

                </td>

                <td width="9%">

                  <div align="center">จำนวนบัญชี</div>

                </td>

                <td width="9%">

                  <div align="center">หุ้น</div>

                </td>

                <td width="9%">

                  <div align="center">อนุพันธ์</div>

                </td>

                <td width="10%">

                  <div align="center">SET SUM</div>

                </td>

              </tr>

              <tr bgcolor="#F7DDFF">

                <td><div align="center">ผู้ลงทุนเก่า</div></td>

                <td><div align="center"></div></td>

                <td><div align="center"></div></td>

                <td><div align="center"></div></td>

                <td bgcolor="#EFEFEF"><div align="center"></div></td>

              </tr>

            ';

    if ($sum_old_cust == "" || $sum_old_cust == 0) {
        echo '<tr>

					<td colspan = "5"><div align="center">ไม่มีผู้ลงทุนเก่าในงานนี้</div></td>

				 </tr>

			';

    } else {
        for ($i = 0; $i < $round; $i++) {
            echo '

				  <tr bgcolor="#F7DDFF">

					<td><div align="center">' . $date[$i] . '</div></td>

					<td><div align="center">' . $old_cust_arr[$i] . '</div></td>

					<td><div align="center">' . $old_stock_arr[$i] . '</div></td>

					<td><div align="center">' . $old_futures_arr[$i] . '</div></td>

					<td bgcolor="#EFEFEF"><div align="center">' . $old_set_sum[$i] . '</div></td>

				  </tr>

				';

        }

        echo '

				  <tr bgcolor="#E9B9FF">

					<td><div align="center">ผลรวม</div></td>

					<td><div align="center">' . $sum_old_cust . '</div></td>

					<td><div align="center">' . $sum_old_stock . '</div></td>

					<td><div align="center">' . $sum_old_futures . '</div></td>

					<td bgcolor="#DDDDDD"><div align="center">' . $sum_old_set . '</div></td>

				  </tr>

				';

    }



    echo '

              <tr bgcolor="#FDEEB5">

                <td><div align="center">ผู้ลงทุนใหม่</div></td>

                <td><div align="center"></div></td>

                <td><div align="center"></div></td>

                <td><div align="center"></div></td>

                <td bgcolor="#EFEFEF"><div align="center"></div></td>

              </tr>

            ';

    if ($sum_new_cust == "" || $sum_new_cust == 0) {
        echo '<tr>

					<td colspan = "5"><div align="center">ไม่มีผู้ลงทุนใหม่ในงานนี้</div></td>

				 </tr>

			';

    } else {
        for ($i = 0; $i < $round; $i++) {
            echo '

				  <tr bgcolor="#FDEEB5">

					<td><div align="center">' . $date[$i] . '</div></td>

					<td><div align="center">' . $new_cust_arr[$i] . '</div></td>

					<td><div align="center">' . $new_stock_arr[$i] . '</div></td>

					<td><div align="center">' . $new_futures_arr[$i] . '</div></td>

					<td bgcolor="#EFEFEF"><div align="center">' . $new_set_sum[$i] . '</div></td>

				  </tr>

				';

        }

        echo '

				  <tr  bgcolor="#FBDF73">

					<td><div align="center">ผลรวม</div></td>

					<td><div align="center">' . $sum_new_cust . '</div></td>

					<td><div align="center">' . $sum_new_stock . '</div></td>

					<td><div align="center">' . $sum_new_futures . '</div></td>

					<td bgcolor="#DDDDDD"><div align="center">' . $sum_new_set . '</div></td>

				  </tr>

				';

    }

    echo '

              <tr bgcolor="#CCCCCC">

                <td><div align="center">ผลรวมทั้งหมด</div></td>

                <td><div align="center">' . $cust . '</div></td>

                <td><div align="center">' . $stock . '</div></td>

                <td><div align="center">' . $futures . '</div></td>

                <td><div align="center">' . $sum_sum_set . '</div></td>

              </tr>

            ';

} else if ($what == "funds") {
    $cust = 0;

    $eq = 0;

    $fix = 0;

    $mix = 0;

    $fif = 0;

    $prop = 0;

    $ltf = 0;

    $rmf = 0;

    $sql_dfd = "SELECT DISTINCT `date` FROM `funds` WHERE `project` = '" . $project . "' and `pro` = '" . $pro . "'";
    try {
        $getDisitinctFundDateQuery = $pdo->query($sql_dfd);
    }
    catch (PDOExeption $e) {
        die("Query failed: " . $e . getMessage());
    }
    $disitinctFundDateQuery = $getDisitinctFundDateQuery->fetchAll();

    $round = 0;

    foreach ($disitinctFundDateQuery as $rs) {
        $old_cust = 0;

        $old_eq = 0;

        $old_fix = 0;

        $old_mix = 0;

        $old_fif = 0;

        $old_prop = 0;

        $old_ltf = 0;

        $old_rmf = 0;

        $new_cust = 0;

        $new_eq = 0;

        $new_fix = 0;

        $new_mix = 0;

        $new_fif = 0;

        $new_prop = 0;

        $new_ltf = 0;

        $new_rmf = 0;

        $sql_cfd = "SELECT * from `funds` WHERE `pro` = '" . $pro . "' and `project` = '" . $project . "' and `date` = '" . $rs['date'] . "'";

        try {
            $getCompareFundDateQuery = $pdo->query($sql_cfd);
        }
        catch (PDOExeption $e) {
            die("Query failed: " . $e . getMessage());
        }
        $compareFundDateResults = $getCompareFundDateQuery->fetchAll();

        foreach ($compareFundDateResults as $rs2) {
            $sql_funds = "select * from `funds` where id = '" . $rs2['id'] . "' and pro != '" . $pro . "' and project != '" . $project . "' and `date` != '" . $rs['date'] . "'";

            try {
                $getFundsQuery = $pdo->query($sql_funds);
            }
            catch (PDOExeption $e) {
                die("Query failed: " . $e . getMessage());
            }
            $rs = $getFundsQuery->fetchAll();

            $num_row3 = count($rs);

            if ($num_row3 != 0) {
                $old_cust++;

                if ($rs['eq'] == "1") {
                    $old_eq++;

                }

                if ($rs['fix'] == "1") {
                    $old_fix++;

                }

                if ($rs['mix'] == "1") {
                    $old_mix++;

                }

                if ($rs['fif'] == "1") {
                    $old_fif++;

                }

                if ($rs['prop'] == "1") {
                    $old_prop++;

                }

                if ($rs['ltf'] == "1") {
                    $old_ltf++;

                }

                if ($rs['rmf'] == "1") {
                    $old_rmf++;

                }

            } else {
                $new_cust++;

                if ($rs['eq'] == "1") {
                    $new_eq++;

                }

                if ($rs['fix'] == "1") {
                    $new_fix++;

                }

                if ($rs['mix'] == "1") {
                    $new_mix++;

                }

                if ($rs['fif'] == "1") {
                    $new_fif++;

                }

                if ($rs['prop'] == "1") {
                    $new_prop++;

                }

                if ($rs['ltf'] == "1") {
                    $new_ltf++;

                }

                if ($rs['rmf'] == "1") {
                    $new_rmf++;

                }

            }

        }

        $old_cust_arr[$round] = $old_cust;

        $new_cust_arr[$round] = $new_cust;

        $old_eq_arr[$round] = $old_eq;

        $new_eq_arr[$round] = $new_eq;

        $old_fix_arr[$round] = $old_fix;

        $new_fix_arr[$round] = $new_fix;

        $old_mix_arr[$round] = $old_mix;

        $new_mix_arr[$round] = $new_mix;

        $old_fif_arr[$round] = $old_fif;

        $new_fif_arr[$round] = $new_fif;

        $old_prop_arr[$round] = $old_prop;

        $new_prop_arr[$round] = $new_prop;

        $old_ltf_arr[$round] = $old_ltf;

        $new_ltf_arr[$round] = $new_ltf;

        $old_rmf_arr[$round] = $old_rmf;

        $new_rmf_arr[$round] = $new_rmf;



        $date[$round] = $rs['date'];

        $round++;

    }

    for ($i = 0; $i < $round; $i++) {
        $sum_old_cust += $old_cust_arr[$i];

        $sum_old_eq += $old_eq_arr[$i];

        $sum_old_fix += $old_fix_arr[$i];

        $sum_old_mix += $old_mix_arr[$i];

        $sum_old_fif += $old_fif_arr[$i];

        $sum_old_prop += $old_prop_arr[$i];

        $sum_old_ltf += $old_ltf_arr[$i];

        $sum_old_rmf += $old_rmf_arr[$i];



        $sum_new_cust += $new_cust_arr[$i];

        $sum_new_eq += $new_eq_arr[$i];

        $sum_new_fix += $new_fix_arr[$i];

        $sum_new_mix += $new_mix_arr[$i];

        $sum_new_fif += $new_fif_arr[$i];

        $sum_new_prop += $new_prop_arr[$i];

        $sum_new_ltf += $new_ltf_arr[$i];

        $sum_new_rmf += $new_rmf_arr[$i];



        $old_funds_sum[$i] = $old_cust_arr[$i] + $old_eq_arr[$i] + $old_fix_arr[$i] + $old_mix_arr[$i] + $old_fif_arr[$i] + $old_prop_arr[$i] + $old_ltf_arr[$i] + $old_ltf_arr[$i] + $old_rmf_arr[$i];

        $new_funds_sum[$i] = $new_cust_arr[$i] + $new_eq_arr[$i] + $new_fix_arr[$i] + $new_mix_arr[$i] + $new_fif_arr[$i] + $new_prop_arr[$i] + $new_ltf_arr[$i] + $new_ltf_arr[$i] + $new_rmf_arr[$i];



        $sum_old_funds += $old_funds_sum[$i];

        $sum_new_funds += $new_funds_sum[$i];



        $cust = $sum_old_cust + $sum_new_cust;

        $eq = $sum_old_eq + $sum_new_eq;

        $fix = $sum_old_fix + $sum_new_fix;

        $mix = $sum_old_mix + $sum_new_mix;

        $fif = $sum_old_fif + $sum_new_fif;

        $prop = $sum_old_prop + $sum_new_prop;

        $ltf = $sum_old_ltf + $sum_new_ltf;

        $rmf = $sum_old_rmf + $sum_new_rmf;

    }

    echo '

				<tr bgcolor="#CCFFFF">

                <td width="10%" height="25">

                  <div align="center"></div>

                </td>

                <td width="10%" height="25">

                  <div align="center">จำนวนบัญชี</div>

                </td>

                <td width="10%" height="25">

                  <div align="center">หุ้น</div>

                </td>

                <td width="10%" height="25">

                  <div align="center">ตราสารหนี้</div>

                </td>

                <td height="25" width="10%">

                  <div align="center">ผสม</div>

                </td>

                <td height="25" width="10%">

                  <div align="center">FIF</div>

                </td>

                <td height="25" width="10%">

                  <div align="center">Prop.</div>

                </td>

                <td height="25" width="10%">

                  <div align="center">LTF</div>

                </td>

                <td height="25" width="10%">

                  <div align="center">RMF</div>

                </td>

				<td height="25" width="10%">

				  <div align="center">funds sum</div>

				</td>

              </tr>

              <tr bgcolor="#F7DDFF">

                <td width="10%">

                  <div align="center">นักลงทุนเก่า</div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

				<td width="10%" bgcolor="#EFEFEF">

				  <div align="center"></div>

				</td>

              </tr>';

    if ($sum_old_cust == "" || $sum_old_cust == 0) {
        echo '<tr>

						<td colspan = "10"><div align="center">ไม่มีผู้ลงทุนเก่าในงานนี้</div></td>

					 </tr>

				';

    } else {
        for ($i = 0; $i < $round; $i++) {
            echo '

				<tr bgcolor="#F7DDFF">

                <td width="10%">

                  <div align="center">' . $date[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $old_cust_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $old_eq_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $old_fix_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $old_mix_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $old_fif_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $old_prop_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $old_ltf_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $old_rmf_arr[$i] . '</div>

                </td>

				<td width="10%" bgcolor="#EFEFEF">

				  <div align="center">' . $old_funds_sum[$i] . '</div>

				</td>

				</tr>';

        }

        echo '

				<tr bgcolor="#E9B9FF">

				<td width="10%">

                  <div align="center">ผลรวม</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_old_cust . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_old_eq . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_old_fix . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_old_mix . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_old_fif . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_old_prop . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_old_ltf . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_old_rmf . '</div>

                </td>

				<td width="10%" bgcolor="#DDDDDD">

				  <div align="center">' . $sum_old_funds . '</div>

				</td>

				</tr>';

    }

    echo '

			    <tr bgcolor="#FDEEB5">

                <td width="10%">

                  <div align="center">ผู้ลงทุนใหม่</div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

                <td width="10%">

                  <div align="center"></div>

                </td>

				<td width="10%" bgcolor="#EFEFEF">

				  <div align="center"></div>

				</td>

              </tr>';

    if ($sum_new_cust == "" || $sum_new_cust == 0) {
        echo '<tr>

						<td colspan = "10"><div align="center">ไม่มีผู้ลงทุนใหม่ในงานนี้</div></td>

					 </tr>

				';

    } else {
        for ($i = 0; $i < $round; $i++) {
            echo '

				<tr bgcolor="#FDEEB5">

                <td width="10%">

                  <div align="center">' . $date[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $new_cust_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $new_eq_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $new_fix_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $new_mix_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $new_fif_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $new_prop_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $new_ltf_arr[$i] . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $new_rmf_arr[$i] . '</div>

                </td>

				<td width="10%" bgcolor="#EFEFEF">

				  <div align="center">' . $old_funds_sum[$i] . '</div>

				</td>

				</tr>';

        }

        echo '

				<tr  bgcolor="#FBDF73">

				<td width="10%">

                  <div align="center">ผลรวม</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_new_cust . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_new_eq . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_new_fix . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_new_mix . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_new_fif . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_new_prop . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_new_ltf . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $sum_new_rmf . '</div>

                </td>

				<td width="10%" bgcolor="#DDDDDD">

				  <div align="center">' . $sum_new_funds . '</div>

				</td>

				</tr>';

    }

    echo '

              <tr bgcolor="#CCCCCC">

                <td width="10%">

                  <div align="center">ผลรวมทั้งหมด</div>

                </td>

                <td width="10%">

                  <div align="center">' . $cust . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $eq . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $fix . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $mix . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $fif . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $prop . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $ltf . '</div>

                </td>

                <td width="10%">

                  <div align="center">' . $rmf . '</div>

                </td>

				<td width="10%">

				  <div align="center"></div>

				</td>

              </tr>

			';

}

?>

			</table>

          </td>

        </tr>

      </table>

</td>

    <td width="4" background="images/shadowpoint.jpg"></td>

  </tr>

  <tr>

    <td height="1" background="images/point.jpg"></td>

    <td height="1" background="images/point.jpg"></td>

    <td width = "4" height="1" background="images/shadowpoint.jpg"></td>

  </tr>

</table>

<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

        <form name="form1" method="post" action="<?php
echo htmlentities($_SERVER['PHP_SELF']);
?>">



      <td width="104">

        <input type="submit" name="back" value="back">

      </td>

	      </form>

  </tr>

  <tr>

    <td colspan="2">



    </td>

  </tr>

</table>

</body>

</html>

