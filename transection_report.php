<?php

require_once("connect.inc.php");
require_once("auth.inc.php");

if (isset($_POST['back'])) {
    header("Location: main_report.php");

}

$what = get_session_or_cookie('what');
$pro = get_session_or_cookie('pro');
$project = get_session_or_cookie('project');

if ($what  == "") {
  header("Location: index.php");
}

if ($project == "") {
  header("Location: main.php");
}

if ($what == "funds") {
  if ($pro == "") {
      header("Location: choose.php");
  }
}

$sql = "select `project` from `project` where `status` = 'Y' and `type` ='set'";
try {
    $getQuery = $pdo->query($sql);
}
catch (PDOExeption $e) {
    die("Query failed: " . $e . getMessage());
}
$queryResults = $getQuery->fetchAll();

?>

<DOCTYPE! html>

<html>

<head>

    <meta charset="utf-8">

    <link rel="stylesheet" href="css/transection_report.css">

    <script src="js/jquery-3.1.0.min.js"></script>

</head>

<body>



<form action="">

<select name="project" id="project" onchange="showDate(this.value)">

<option value="">Select a Project:</option>

<?php
//call project list
foreach ($queryResults as $rs) {
echo <<<EOT
<option value="$rs[project]">$rs[project]</option>

EOT;
}
?>
</select>



<p id="txtHintDate"></p>

<input type="hidden" name="date" id="date" value="">

<br/><br/>



<div id="search_form" style="display:none;">

<form action="">

<input type="text" name="keyword" id="keyword" value="" size="20" placeholder="search">

<select name="cat" id="cat">

    <option value="">Please select a category:</option>

    <option value="fname">ชื่อ</option>

    <option value="sname">นามสกุล</option>

    <option value="mobile">มือถือ</option>

    <option value="email">อีเมล</option>

    <option value="time">เวลา</option>

    <option value="sec_name">บริษัทหลักทรัพย์</option>

    <option value="redeemer_name">ชื่อผู้แลก</option>

    <option value="set_voucher_id">เลขที่ Voucher หุ้น</option>

    <option value="tfex_voucher_id">เลขที่ Voucher อนุพันธ์</option>

    <option value="double_voucher_id">เลขที่ Voucher คู่</option>

    <option value="b2b_voucher_id">เลขที่ Voucher AomWise</option>

</select><br/><br/>



<label><input type="checkbox" name="toggle_select" id="toggle_select" value="1">เลือก / ยกเลิก ทั้งหมด</label><br/>

<p>เปิดบัญชี&nbsp;</p>

<label><input type="checkbox" name="stock" id="stock" value="1">หุ้น</label>

<label><input type="checkbox" name="futures" id="futures" value="1">อนุพันธ์</label>

<label><input type="checkbox" name="mf" id="mf" value="1">กองทุนออนไลน์</label><br/>

<p>รับ Voucher&nbsp;</p>

<label><input type="checkbox" name="set_voucher" id="set_voucher" value="1">หุ้น</label>

<label><input type="checkbox" name="tfex_voucher" id="tfex_voucher" value="1">อนุพันธ์</label>

<label><input type="checkbox" name="double_voucher" id="double_voucher" value="1">บัญชีคู่</label>

<label><input type="checkbox" name="b2b_voucher" id="b2b_voucher" value="1">บัญชี AomWise</label>

<br/>

<p>ผู้แลก&nbsp;</p>

<label><input type="radio" name="redeemer" id="none" value="" checked>None</label>

<label><input type="radio" name="redeemer" id="customer" value="customer">ลูกค้า</label>

<label><input type="radio" name="redeemer" id="marketing" value="marketing">Marketing</label>&nbsp;

<input type="button" name="search" value="เริ่มการค้นหา" onClick="showReport()">

</form>

</div>



<br/>

<div id="txtHint1">Customer info will be listed here...</div>

<br/>

<form name="form1" method="post" action="<?php
echo htmlentities($_SERVER['PHP_SELF']);
?>">

    <input type="submit" name="back" value="back">

</form>





<script>



$('#toggle_select').click(function() {

    var checkedStatus = this.checked;

    $('label').find(':checkbox').each(function() {

        $(this).prop('checked', checkedStatus);

    });

});



//Check KEY ENTER

$(document).ready(function() {

    $(window).keydown(function(event){

        if(event.keyCode == 13) {

            event.preventDefault();

            return false;

        }

    });

});



function setReport(str) {

    $('#date').val(str);

    if(str !== ""){

        $('#search_form').show();

    }else{

        $('#search_form').hide();

    }

    showReport();

}

//Ajax Script Show Date

function showDate(str) {

    var xhttpDate;

    document.getElementById("txtHintDate").innerHTML = "Loading...";

    if (str == "") {

        document.getElementById("txtHintDate").innerHTML = "";

        return;

    }

    xhttpDate = new XMLHttpRequest();

    xhttpDate.onreadystatechange = function() {

        if (this.readyState == 4 && this.status == 200) {

            document.getElementById("txtHintDate").innerHTML = this.responseText;

        }

    };

    xhttpDate.open("GET", "transection_report_date_ajax.php?project="+str, true);

    xhttpDate.send();

}



//Ajax Script Show Report

function showReport() {

    var xhttp1;

    var project = $('#project').val();

    //alert(project);

    var date = $('#date').val();

    //alert(date);

    var keyword = $("#keyword").val();

    var cat = $("#cat").val();

    //check ได้ True ทำถึงตรงนี้

    var stock = $("#stock").prop('checked');

    var futures = $("#futures").prop('checked');

    var mf = $("#mf").prop('checked');

    var set_voucher = $("#set_voucher").prop('checked');

    var tfex_voucher = $("#tfex_voucher").prop('checked');

    var double_voucher = $("#double_voucher").prop('checked');

    var b2b_voucher = $("#b2b_voucher").prop('checked');

    var redeemer;

    if($("#none").prop("checked")){

        redeemer = "";

    }else if($("#customer").prop("checked")){

        redeemer = "customer";

    }else if($("#marketing").prop("checked")){

        redeemer = "marketing";

    }

    console.log($("#none").prop("checked"));

    console.log($("#customer").prop("checked"));

    console.log($("#marketing").prop("checked"));

    console.log(mf);

    console.log(redeemer);



    console.log("openStock : "+stock+" openFutures : "+futures+" openMf : "+mf+" set_voucher : "+set_voucher+" tfex_voucher : "+tfex_voucher+" double_voucher : "+double_voucher+" b2b_voucher : "+b2b_voucher);



    document.getElementById("txtHint1").innerHTML = "Loading...<br/>";

    /*if (str == "") {

        document.getElementById("txtHint1").innerHTML = "";

        return;

    }*/

    xhttp1 = new XMLHttpRequest();

    xhttp1.onreadystatechange = function() {

        if (this.readyState == 4 && this.status == 200) {

            document.getElementById("txtHint1").innerHTML = "Information complete.";

            document.getElementById("txtHint1").innerHTML += this.responseText;

        }

    };

    xhttp1.open("GET", "transection_report_ajax.php?project="+project+"&date="+date+"&keyword="+keyword+"&cat="+cat+"&stock="+stock+"&futures="+futures+"&mf="+mf+"&set_voucher="+set_voucher+"&tfex_voucher="+tfex_voucher+"&double_voucher="+double_voucher+"&b2b_voucher="+b2b_voucher+"&redeemer="+redeemer+"", true);

    xhttp1.send();

}

</script>



</body>

</html>