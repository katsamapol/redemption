$(document).ready(function ()
{
    $("#email").keydown(function(e){
        var key = e.key;
        var myRegEx  = /^[\u0E00-\u0E7F]+$/i;
        var isValid = !(myRegEx.test(key));
        if(isValid == false){
            return false;
        }
        console.log(isValid);
    });
    $("#reader").click(function(){
        if ($('#reader').is(':checked')){
            //checked return true
            console.log("reader checked");
            $("#idCard").focus();
            $("#lockBoard").val("2");
        }else{
            //not checked return false
            console.log("not check");
            $("#idCard").focus();
            $("#lockBoard").val("0");
        }
    });

    $("#mainBody").keydown(function(e) {
        if($("#lockBoard").val() == 1){
            console.log("LOCK ANY KEYPRESS");
            e.preventDefault();
        }
    });
    $("#mainBody").mousedown(function(e) {
        if($("#lockBoard").val() == 1 || $("#lockBoard").val() == 2){
            console.log("LOCK ANY MOUSE CLICK");
            e.preventDefault();
        }
    });
    $("#investment_type").keydown(function(e) {
        if($("#lockBoard").val() == 1){
            console.log("LOCK ANY KEYPRESS");
            e.preventDefault();
        }
    });
    $("#investment_type").click(function(e) {
        if($("#lockBoard").val() == 1 || $("#lockBoard").val() == 2){
            console.log("LOCK ANY MOUSE CLICK");
            e.preventDefault();
        }
    });

    $("#hidden_id").change(function ()
    {
        var txt = $("#txtHint").text();
        if (txt.search("true") != -1)
        {
            var fname = $("#fname").val();
            var sname = $("#sname").val();
            var mobile = $("#mobile").val();
            var dob = $("#dob").val();
            var mob = $("#mob").val();
            var yob = $("#yob").val();
            var sex = $("#sexText").val();
            var address = $("#address").val();
            var provinceText = $("#provinceText").val();
            var email = $("#email").val();

            if ($('#reader').is(':checked'))
            {
                setTimeout(function ()
                {
                    setTimeout(function ()
                    {
                        if (mobile == "")
                        {
                            showOldInfo("mobile");
                        }
                    }, 0);
                    setTimeout(function ()
                    {
                        if (email == "")
                        {
                            showOldInfo("email");
                        }
                    }, 1000);
                    setTimeout(function ()
                    {
                            $("#lockBoard").val("0");
                            console.log("Lockboard", $("#lockBoard").val());
                    }, 11000);
                }, 0);
            }
            else
            {
                $("#lockBoard").val("1");
                console.log("Lockboard", $("#lockBoard").val());
                setTimeout(function ()
                {
                    setTimeout(function (){
                        if (fname == "")
                        {
                            showOldInfo("fname");
                        }
                    }, 0);
                    setTimeout(function ()
                    {
                        if (sname == "")
                        {
                            showOldInfo("sname");
                        }
                    }, 0);
                    // }, 1000);
                    setTimeout(function ()
                    {
                        if (dob == "")
                        {
                            showOldInfo("dob");
                        }
                    }, 0);
                    // }, 1350);
                    setTimeout(function ()
                    {
                        if (mob == "")
                        {
                            showOldInfo("mob");
                        }
                    }, 0);
                    // }, 1700);
                    setTimeout(function ()
                    {
                        if (yob == "")
                        {
                            showOldInfo("yob");
                        }
                    }, 0);
                    // }, 2000);
                    setTimeout(function ()
                    {
                        if (sex == "")
                        {
                            showOldInfo("sex");
                        }
                    }, 0);
                    // }, 3000);
                    setTimeout(function ()
                    {
                        if (address == "")
                        {
                            showOldInfo("address");
                        }
                    }, 0);
                    // }, 4000);
                    setTimeout(function ()
                    {
                        if (provinceText == "")
                        {
                            showOldInfo("province");
                        }
                    }, 0);
                    // }, 5000);
                    setTimeout(function ()
                    {
                        if (mobile == "")
                        {
                            showOldInfo("mobile");
                        }
                    }, 0);
                    // }, 6000);
                    setTimeout(function ()
                    {
                        if (email == "")
                        {
                            showOldInfo("email");
                        }
                    }, 0);
                    // }, 7000);
                    setTimeout(function ()
                    {
                        $("#lockBoard").val("0");
                        console.log("Lockboard", $("#lockBoard").val());
                    }, 500);
                    // }, 7000);
                }, 0);
            }
        }
        else
        {}

    }
    );

    $("#toggle_id").click(function ()
    {

        console.log($("#id_type").text());

        if ($("#id_type").text() == "passport")
        {

            $("#id_type").text("idcard");

            $("#id_type2").text("เลขที่บัตรประชาชน");

            console.log($("#id_type").text());

        }
        else
        {

            $("#id_type").text("passport");

            $("#id_type2").text("Passport");

            console.log($("#id_type").text());

        }

    }
    );

    $("#stock").click(function ()
    {

        $("#set_voucher_label_area").toggle();
        // $("#b2b_voucher_label_area").toggle();
        
        //if($("#stock").prop("checked") && $("#futures").prop("checked")){

        if ($("#stock").prop("checked") || $("#futures").prop("checked"))
        {

            $("#double_voucher_label_area").show();

            //$("#b2b_voucher_label_area").show();

        }
        else
        {

            $("#double_voucher_label_area").hide();

            //$("#b2b_voucher_label_area").hide();

        }

    }
    );

    $("#futures").click(function ()
    {

        $("#tfex_voucher_label_area").toggle();

        //if($("#stock").prop("checked") && $("#futures").prop("checked")){

        if ($("#futures").prop("checked") || $("#stock").prop("checked"))
        {

            $("#double_voucher_label_area").show();

            //$("#b2b_voucher_label_area").show();

        }
        else
        {

            $("#double_voucher_label_area").hide();

            //$("#b2b_voucher_label_area").hide();

        }

    }
    );

    $("#set_voucher_label").click(function ()
    {

        $("#set_voucher_area").toggle();

        $("#set_voucher_id_subfix").val("");

        showVoucher("set_voucher_id_prefix","set");

    }
    );

    $("#tfex_voucher_label").click(function ()
    {

        $("#tfex_voucher_area").toggle();

        $("#tfex_voucher_id_subfix").val("");

        showVoucher("tfex_voucher_id_prefix","tfex");

    }
    );

    $("#double_voucher_label").click(function ()
    {

        $("#double_voucher_area").toggle();

        $("#double_voucher_id_subfix").val("");

        showVoucher("double_voucher_id_prefix","double");

    }
    );

    $("#b2b_voucher_label").click(function ()
    {

        $("#b2b_voucher_area").toggle();

        $("#b2b_voucher_id_subfix").val("");

        showVoucher("b2b_voucher_id_prefix","other");

    }
    );

    $("#refresh_set_voucher").click(function ()
    {

        $("#set_voucher_id_subfix").val("");

        showVoucher("set_voucher_id_prefix","set");

    }
    );

    $("#refresh_tfex_voucher").click(function ()
    {

        $("#tfex_voucher_id_subfix").val("");

        showVoucher("tfex_voucher_id_prefix","tfex");

    }
    );

    $("#refresh_double_voucher").click(function ()
    {

        $("#double_voucher_id_subfix").val("");

        showVoucher("double_voucher_id_prefix","double");

    }
    );

    $("#refresh_b2b_voucher").click(function ()
    {

        $("#b2b_voucher_id_subfix").val("");

        showVoucher("b2b_voucher_id_prefix","other");

    }
    );

    $("#set_voucher_id_prefix").change(function ()
    {

        showSubfix("set");

    }
    );

    $("#tfex_voucher_id_prefix").change(function ()
    {

        showSubfix("tfex");

    }
    );

    $("#double_voucher_id_prefix").change(function ()
    {

        showSubfix("double");

    }
    );

    $("#b2b_voucher_id_prefix").change(function ()
    {

        showSubfix("other");

    }
    );

    $("#sec_name").change(function ()
    {

        var sec_name = $('#sec_name').val();

        var redeemer = $('#redeemer').val();

        if (((typeof(sec_name) !== "undefined") || (typeof(redeemer) !== "undefined")) && (redeemer == "marketing"))
        {
            showRedeemer(sec_name, redeemer);
        }

    }
    );

    $("#asset_name").change(function ()
    {

        var asset_name = $('#asset_name').val();

        var redeemer = $('#redeemer').val();

        if (((typeof(asset_name) !== "undefined") || (typeof(redeemer) !== "undefined")) && (redeemer == "marketing"))
        {
            showRedeemer(asset_name, redeemer);
        }

    }
    );

    $("#redeemer").change(function ()
    {
        if($(this).val() == "customer"){
            $('#redeemer_name').prop('readonly', true);
            $('#redeemer_name').val("-");
        }else{
            $('#redeemer_name').prop('readonly', false);
            $('#redeemer_name').val("");
        }

        if ($('#sec_name').val())
        {
            var com_name = $('#sec_name').val();
        }

        if ($('#asset_name').val())
        {
            var com_name = $('#asset_name').val();
        }

        var redeemer = $('#redeemer').val();
        //console.log(redeemer);

        if (((typeof(com_name) !== "undefined") || (typeof(redeemer) !== "undefined")) && (redeemer == "marketing"))
        {
            showRedeemer(com_name, redeemer);
        }

    }
    );

    $('form').submit(function ()  {}

    );

    /*$("#investment_type input[type=checkbox]").change(function(){

    console.log($(this).prop("value"));

    console.log($(this).prop("checked"));

    });*/

    //check form Submit

    $("#submit_redemption").click(function ()
    {

        var check_ok = "true";

        var set_voucher_id_prefix = $('#set_voucher_id_prefix').val();

        var tfex_voucher_id_prefix = $('#tfex_voucher_id_prefix').val();

        var double_voucher_id_prefix = $('#double_voucher_id_prefix').val();

        var b2b_voucher_id_prefix = $('#b2b_voucher_id_prefix').val();

        var set_voucher_id_subfix = $('#set_voucher_id_subfix').val();

        var tfex_voucher_id_subfix = $('#tfex_voucher_id_subfix').val();

        var double_voucher_id_subfix = $('#double_voucher_id_subfix').val();

        var b2b_voucher_id_subfix = $('#b2b_voucher_id_subfix').val();

        var redeemer = $('#redeemer').val();

        var redeemer_name = $('#redeemer_name').val();

        var sec_name = $('#sec_name').val();

        var asset_name = $('#asset_name').val();

        var mobile = $('#mobile').val();

        var email = $('#email').val();

        var investment_type = "false";

        //alert("secname is "+sec_name);

        //alert("assetname is "+asset_name);

        //alert("'"+set_voucher_id+"'");


        // Check if empty or not

        if ((sec_name !== "") && (asset_name === undefined))
        {

            //Leave it True

        }
        else if ((sec_name === undefined) && (asset_name !== ""))
        {

            //Leave it True

        }
        else
        {

            check_ok = "false";

            if (sec_name === "")
            {

                $('#sec_name').focus();

            }

            if (asset_name === "")
            {

                $('#asset_name').focus();

            }

            alert("กรุณาเลือกบริษัท");

            return false;

        }

        if (mobile !== "")
        {

            //Leave it True

        }
        else
        {

            check_ok = "false";

            $('#mobile').focus();

            alert("กรุณาระบุหมายเลขโทรศัพท์มือถือ");

            return false;

        }

        if (email !== "")
        {

            //Leave it True

        }
        else
        {

            check_ok = "false";

            $('#email').focus();

            alert("กรุณาระบุ Email");

            return false;

        }

        if (redeemer !== "")
        {

            //Leave it True

        }
        else
        {

            check_ok = "false";

            $('#redeemer').focus();

            alert("กรุณาเลือกผู้รับ");

            return false;

        }

        if (redeemer_name !== "")
        {

            //Leave it True

        }
        else
        {

            check_ok = "false";

            $('#redeemer_name').focus();

            alert("กรุณาใส่ชื่อผู้รับ");

            return false;

        }

        //loop into investment_type and for for checkbox

        $("#investment_type input[type=checkbox]").each(function ()
        {

            var sThisVal = (this.checked ? "1" : "0");

            if (sThisVal == "1")
            {

                investment_type = "ok";

                return false;

            }
            else
            {

                investment_type = "false";

            }

        }
        );

        if (investment_type == "ok")
        {

            check_ok = "true";

        }
        else
        {

            check_ok = "false";

            alert("กรุณาระบุประเภทการลงทุน");

        }

        if ($('#set_voucher_label').prop('checked'))
        {
            if ((set_voucher_id_prefix != "") && (set_voucher_id_subfix != "") && (set_voucher_id_subfix != "หมด"))
            {
                //Leave it True
            }
            else
            {
                check_ok = "false";
                if (set_voucher_id_prefix === "")
                {

                    $('#set_voucher_id_prefix').focus();

                }
                else if (set_voucher_id_subfix === "")
                {

                    $('#set_voucher_id_subfix').focus();

                }
                else
                {}

                if (set_voucher_id_subfix == "หมด")
                {

                    alert("Voucher หมด");

                }
                else
                {

                    alert("กรุณาระบุเลขที่ Voucher");

                }

                return false;

            }

        }

        if ($('#tfex_voucher_label').prop('checked'))
        {

            if ((tfex_voucher_id_prefix != "") && (tfex_voucher_id_subfix != "") && (tfex_voucher_id_subfix != "หมด"))
            {

                //Leave it True

            }
            else
            {

                check_ok = "false";

                if (tfex_voucher_id_prefix == "")
                {

                    $('#tfex_voucher_id_prefix').focus();

                }
                else if (tfex_voucher_id_subfix == "")
                {

                    $('#tfex_voucher_id_subfix').focus();

                }
                else
                {}

                if (tfex_voucher_id_subfix == "หมด")
                {

                    alert("Voucher หมด");

                }
                else
                {

                    alert("กรุณาระบุเลขที่ Voucher");

                }

                return false;

            }

        }

        if ($('#double_voucher_label').prop('checked'))
        {
            if ((double_voucher_id_prefix != "") && (double_voucher_id_subfix != "") && (double_voucher_id_subfix != "หมด"))
            {

                //Leave it True

            }
            else
            {

                check_ok = "false";

                if (double_voucher_id_prefix == "")
                {

                    $('#double_voucher_id_prefix').focus();

                }
                else if (double_voucher_id_subfix == "")
                {

                    $('#double_voucher_id_subfix').focus();

                }
                else
                {}

                if (double_voucher_id_subfix == "หมด")
                {

                    alert("Voucher หมด");

                }
                else
                {

                    alert("กรุณาระบุเลขที่ Voucher");

                }

                return false;

            }

        }

        if ($('#b2b_voucher_label').prop('checked'))
        {

            if ((b2b_voucher_id_prefix != "") && (b2b_voucher_id_subfix != "") && (b2b_voucher_id_subfix != "หมด"))
            {

                //Leave it True

            }
            else
            {

                check_ok = "false";

                if (b2b_voucher_id_prefix == "")
                {

                    $('#b2b_voucher_id_prefix').focus();

                }
                else if (b2b_voucher_id_subfix == "")
                {

                    $('#b2b_voucher_id_subfix').focus();

                }
                else
                {}

                if (b2b_voucher_id_subfix == "หมด")
                {

                    alert("Voucher หมด");

                }
                else
                {

                    alert("กรุณาระบุเลขที่ Voucher");

                }

                return false;

            }

        }

        if (check_ok === "true")
        {

            $("#submit_redemption").prop('disabled', true);

            $('#redemption').submit();

        }

    }
    );

}
);

//card reader function

function setIdAndReadOnly(t, b)
{

    document.getElementById('' + b).value = t;

    document.getElementById('' + b).readOnly = true;

}

function check_province(p)
{

    //alert(p);
    var provinceToSelect = p;
    //console.log("before p :"+provinceToSelect);
    if (provinceToSelect == "กรุงเทพมหานคร")
    {
        provinceToSelect = "กรุงเทพ";
    }
    else
    {
        //Do nothing
    }
    //console.log("after p :"+provinceToSelect);

    var sProvince = document.getElementById("province");

    //alert(sProvince.length);

    for (var i = 0; i <= sProvince.length; i++)
    {

        if (sProvince[i].value == provinceToSelect)
        {

            sProvince[i].selected = true;

            //alert(sProvince.options[i].selected);

            display_none('provinceText');

        }

    }

}

function check_sex(p)
{

    //alert(p);

    var sSex = document.getElementById("sex");

    //alert(sSex.length);

    if (p == "น.ส.")
    {

        sSex[2].selected = true;

    }
    else if (p == "นาย")
    {

        sSex[1].selected = true;

    }
    else if (p == "นาง")
    {

        sSex[2].selected = true;

    }
    else if (p == "F")
    {

        sSex[2].selected = true;

    }
    else if (p == "M")
    {

        sSex[1].selected = true;

    }
    else
    {

        sSex[0].selected = true;

    }

    display_none('sexText');

}

function readonly_me(d)
{

    document.getElementById('' + d).readOnly = true;

}

function display_none(d)
{

    document.getElementById('' + d).style.display = "none";

}

//Ajax Script Show Voucher

function showVoucher(where,type)
{

    var xhttpShowVoucher;

    console.log(where);

    document.getElementById(where).innerHTML = "Loading...<br/>";

    /*if (what == "") {

    document.getElementById(where).innerHTML = "";

    return;

    }*/

    xhttpShowVoucher = new XMLHttpRequest();

    xhttpShowVoucher.onreadystatechange = function ()
    {

        if (this.readyState == 4 && this.status == 200)
        {

            document.getElementById(where).innerHTML = this.responseText;

            console.log($("#set_voucher_id_prefix").val());

            console.log($("#set_voucher_id_subfix").val());

        }

    };

    xhttpShowVoucher.open("GET", "getvoucherajax.php?type=" + type, true);

    xhttpShowVoucher.send();

}

//Ajax Script Show Report

function showSubfix(voucher_type)
{

    var xhttpShowSubfix;

    var prefix_value;

    var where;

    if (voucher_type == "set")
    {

        prefix_value = $("#set_voucher_id_prefix").val();

        where = "set_voucher_id_subfix";

    }
    else if (voucher_type == "tfex")
    {

        prefix_value = $("#tfex_voucher_id_prefix").val();

        where = "tfex_voucher_id_subfix";

    }
    else if (voucher_type == "double")
    {

        prefix_value = $("#double_voucher_id_prefix").val();

        where = "double_voucher_id_subfix";

    }
    else if (voucher_type == "b2b")
    {

        prefix_value = $("#b2b_voucher_id_prefix").val();

        where = "b2b_voucher_id_subfix";

    }
    else
    {

        prefix_value = "";

        where = "";

    }

    console.log(prefix_value);

    document.getElementById(where).value = "Loading...";

    if (prefix_value == "")
    {

        document.getElementById(where).value = "";

        return;

    }

    xhttpShowSubfix = new XMLHttpRequest();

    xhttpShowSubfix.onreadystatechange = function ()
    {

        if (this.readyState == 4 && this.status == 200)
        {

            document.getElementById(where).value = this.responseText;

        }

    };

    xhttpShowSubfix.open("GET", "getsubfixajax.php?prefix_value=" + prefix_value, true);

    xhttpShowSubfix.send();

}

//Ajax Script Show Old information

function showOldInfo(val)
{

    var xhttpshowOldInfo;

    var idcard = $("#idCard").val();

    var where = "" + val;

    console.log(idcard);

    console.log(where);

    document.getElementById(where).value = "Loading...";

    if (idcard == "")
    {

        document.getElementById(where).value = "";

        return;

    }

    xhttpshowOldInfo = new XMLHttpRequest();

    xhttpshowOldInfo.onreadystatechange = function ()
    {

        if (this.readyState == 4 && this.status == 200)
        {

            if (where == "province")
            {

                where = "provinceText";

                document.getElementById(where).value = this.responseText;

                document.getElementById(where).focus();

                $('#provinceText').blur();

            }
            else if (where == "sex")
            {

                where = "sexText";

                document.getElementById(where).value = this.responseText;

                document.getElementById(where).focus();

                $('#sexText').blur();

            }
            else
            {

                document.getElementById(where).value = this.responseText;

            }

        }

    };

    xhttpshowOldInfo.open("GET", "getoldinfo.php?where=" + where + "&idcard=" + idcard, true);

    xhttpshowOldInfo.send();

}

//Ajax Script Show Report

function showRedeemer(com_name, redeemer_type)
{

    var xhttpShowRedeemer;

    console.log("showing redeemer");

    document.getElementById("showRedeemerText").innerHTML = "Loading...";

    if ((com_name == "") || (redeemer_type == ""))
    {

        document.getElementById("showRedeemerText").innerHTML = "";

        return;

    }

    xhttpShowRedeemer = new XMLHttpRequest();

    xhttpShowRedeemer.onreadystatechange = function ()
    {

        if (this.readyState == 4 && this.status == 200)
        {

            document.getElementById("showRedeemerText").innerHTML = this.responseText;

        }

    };

    xhttpShowRedeemer.open("GET", "getredeemerajax.php?com_name=" + com_name + "&redeemer_type=" + redeemer_type, true);

    xhttpShowRedeemer.send();

}
