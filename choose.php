<?php
require_once("connect.inc.php");
require_once("auth.inc.php");

// Check if session or cookie is set
$what = get_session_or_cookie('what');
$project = get_session_or_cookie('project');

// Handle form submissions
if (isset($_POST['project'])) {
  $project = $_POST['project'];
  // Set project value based on "Remember Me" status
  set_session_or_cookie("project", $project, isset($_COOKIE['username']));
}

// Handle back button submission
if (isset($_POST['back'])) {
  unset_session_or_cookie('what');
  unset_session_or_cookie('project');
  header("Location: main.php?what=funds");
  exit();
}

// Redirect to main.php if $project is not set after form processing
if (!$project) {
  header("Location: main.php");
  exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Redemption Point</title>
</head>
<body bgcolor="#FFFFFF" text="#000000">

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td height="1" background="images/point.jpg"></td>
        <td height="1" background="images/point.jpg"></td>
        <td width="4" height="1" background="images/shadowpoint.jpg"></td>
    </tr>
    <tr>
        <td width="1" background="images/point.jpg"></td>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">
                        <br>
                        <div align="center">Redemption Point<br>
                            <?php
                            if ($what == "set") {
                                echo '<img src="images/set.jpg">';
                            } elseif ($what == "funds") {
                                echo '<img src="images/mf.jpg">';
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <div align="center"><a href="add.php?pro=pro">ร่วม Promotion</a></div>
                    </td>
                    <td>
                        <div align="center"><a href="add.php?pro=cam">ร่วม Campaign</a></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </td>
        <td width="4" background="images/shadowpoint.jpg"></td>
    </tr>
    <tr>
        <td height="1" background="images/point.jpg"></td>
        <td height="1" background="images/point.jpg"></td>
        <td width="4" height="1" background="images/shadowpoint.jpg"></td>
    </tr>
</table>

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <form name="form1" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
            <td width="104">
                <input type="submit" name="home" value="home">
                <input type="submit" name="back" value="back">
            </td>
        </form>
        <td width="696">&nbsp;</td>
    </tr>
</table>

</body>
</html>