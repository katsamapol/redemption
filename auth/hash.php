<?php

require_once("connect.inc.php");

define('SALT_LENGTH', 20);

function generateHash($plainText, $salt = null){
    if ($salt === null){
        $salt = substr(md5(uniqid(rand(), true)), 0, SALT_LENGTH);
    }else{
        $salt = substr($salt, 0, SALT_LENGTH);
	}
	//echo $salt."<br>";
    return $salt . sha1($salt . $plainText);
}

function checkPassword($user,$pass,$where,$pdo){
	try{
	    $getPasswordQuery = $pdo->query("SELECT `password` FROM `".$where."` WHERE `username`='".$user."'");
	}catch(PDOExeption $e){
	    die("Query failed: ".$e.getMessage());
	}
		$getPasswordResult = $getPasswordQuery->fetch();
		if(isset($getPasswordResult['password'])){
			$salt = substr($getPasswordResult['password'],0,20);
			$enterPass = $salt . sha1($salt . $pass);
			if($getPasswordResult['password'] == $enterPass){
				$check = true;
			}else{
				$check = false;
			}
		}else{
			$check = false;
		}

	return $check;

}



/*

$hash = generateHash("Setstaff2&",null);



echo "$hash<br>";



$salt = substr($hash,0,20);



echo $salt."<br>";



$encrypted_text = substr($hash,20);



echo $encrypted_text;

*/

?>