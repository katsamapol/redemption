<?php

header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
require_once("connect.inc.php");

session_start();

// Function to set session or cookie based on 'Remember Me' status
function set_session_or_cookie($key, $value, $persistent = false) {
    if ($persistent) {
        // Set cookie for persistent storage (8 hours in this example)
		$cookie_duration = 8 * 60 * 60; // 8 hours in seconds
        setcookie($key, $value, time() + $cookie_duration, "/");
    } else {
        // Use session for non-persistent storage
        $_SESSION[$key] = $value;
    }
}

// Function to unset session or cookie
function unset_session_or_cookie($key) {
    // Unset session variable if it exists
    if (isset($_SESSION[$key])) {
        unset($_SESSION[$key]);
    }

    // Unset cookie by setting its expiration time to the past
    if (isset($_COOKIE[$key])) {
        setcookie($key, "", time() - 3600, "/");
        unset($_COOKIE[$key]); // Optional: also unset from $_COOKIE superglobal
    }
}

// Function to get session or cookie value
function get_session_or_cookie($key) {
    return $_SESSION[$key] ?? $_COOKIE[$key] ?? null;
}

//Navigate to home
if(isset($_POST['home'])){
    unset_session_or_cookie('pro');
    unset_session_or_cookie('what');
    unset_session_or_cookie('project');
    header("Location: index.php");
    exit();
}

// Check if session or cookie is set
$username = get_session_or_cookie('username');
if ($username) {
    try {
        // Query the database to check if the username exists
        $getUserQuery = $pdo->prepare("SELECT COUNT(*) FROM `user` WHERE `username` = :username");
        $getUserQuery->bindParam(':username', $username, PDO::PARAM_STR);
        $getUserQuery->execute();
        
        $getUserNumRows = $getUserQuery->fetchColumn();
        
        if ($getUserNumRows != 0) {
            // If the username exists, extend the cookie expiration if "Remember Me" was selected
            if (isset($_COOKIE['username'])) {
                set_session_or_cookie("username", $username, true); // Refresh cookie for 8 more hours
            }
            if (isset($_COOKIE['pro'])) {
                set_session_or_cookie("pro", $_COOKIE['pro'], true); // Refresh cookie for 8 more hours
            }
            if (isset($_COOKIE['what'])) {
                set_session_or_cookie("what", $_COOKIE['what'], true); // Refresh cookie for 8 more hours
            }
            if (isset($_COOKIE['project'])) {
                set_session_or_cookie("project", $_COOKIE['project'], true); // Refresh cookie for 8 more hours
            }
            // User is authenticated; proceed with displaying the protected content here
        } else {
            // If the username does not exist, delete the session or cookie and redirect to login
            unset_session_or_cookie("username");
			unset_session_or_cookie("pro");
			unset_session_or_cookie("what");
			unset_session_or_cookie("project");
            ?>
            <script type='text/javascript'>
                window.location = '/redemption/login.php';
            </script>
            <?php
            exit();
        }
    } catch (PDOException $e) {
        die("Query failed: " . $e->getMessage());
    }
} else {
    // If neither session nor cookie is set, redirect to login
    ?>
    <script type='text/javascript'>
        window.location = '/redemption/login.php';
    </script>
    <?php
    exit();
}

?>
