<?php

require_once("connect.inc.php");
require_once("auth.inc.php");

ini_set('max_execution_time', 3600); //300 seconds = 5 minutes
$time_start = microtime(true);
sleep(1);
if(isset($_POST["searchArea"])){
    $searchArea = $_POST["searchArea"];
}
//print_r($_POST);
if(isset($_POST["eventName"]) != ""){
	//echo "exclude!<br>";
    if(isset($_POST["eOperand"])){
        if($_POST["eOperand"] == "only"){
            $eOperand = "=";
            //echo "operand = <br>";
        }else if($_POST["eOperand"] == "exclude"){
            $eOperand = "!=";
            //echo "operand != <br>";
        }else{
            echo "error event operand is not selected<br>";
            echo "<a href='searchid.php'>Try again</a>";
            exit();
        }
    }
    if(empty($_POST["eOperand"])){
        $eventRules = "";
        //echo "Empty operand<br>";
    }else{
        if($_POST["eventName"] == "setinthecity-1"){
            $eventRules = "AND `project` ".$eOperand." 'SET in the City กรุงเทพ 2017 ครั้งที่ 1'";
        }else if($_POST["eventName"] == "setinthecity-2"){
            $eventRules = "AND `project` ".$eOperand." 'SET in the City กรุงเทพ 2017 ครั้งที่ 2'";
        }else if($_POST["eventName"] == "investnow-2"){
            $eventRules = "AND `project` ".$eOperand." 'investnow กรุงเทพ ตลาดหลักทรัพย์ 2018 ครั้งที่ 2'";
        }else if($_POST["eventName"] == "nf-aws-saraburi-2018"){
            $eventRules = "AND `project` ".$eOperand." 'New Frontier AWS สระบุรี 2018'";
        }
    }
}else{
	$eventRules = "";
}
if(isset($_POST["brokerName"])){
    $brokerName = $_POST["brokerName"];
}else{
    $brokerName = "";
}
if($brokerName != ""){
    //echo "exclude!<br>";
    if(isset($_POST["bOperand"])){
        if($_POST["bOperand"] == "only"){
            $bOperand = "=";
        }else if($_POST["bOperand"] == "exclude"){
            $bOperand = "!=";
        }else{
            echo "error broker operand is not selected<br>";
            echo "<a href='searchid.php'>Try again</a>";
            exit();
        }
    }
    if(empty($_POST["bOperand"])){
        $brokerRules = "";
    }else{
        $brokerRules = "AND `sec_name` ".$bOperand." '".$_POST["brokerName"]."'";
    }
}else{
    $brokerRules = "";
}




if(isset($searchArea)){
    //echo "".$searchArea."<br>";
    $search = $searchArea;
    $search = preg_split('/\r\n|[\r\n]/', $search);
    //print_r($search);
    $comma_separated = "('".implode("','", $search)."')";  // ('1','2','3')
    $i=0;
    $d=0;
    foreach ($search as $value) {
        $i++;
        //if($i%10){
        //    sleep(1);
        //}

       $sql = "SELECT `id` AS `identification` FROM  `set` WHERE MATCH (`id`) AGAINST ('+".$value."' IN BOOLEAN MODE) ".$eventRules." ".$brokerRules."";
	   //echo $sql."<br>";
       //break;
        try{
            $getQuery = $pdo->query($sql);
        }catch(PDOExeption $e){
            die("Query failed: ".$e.getMessage());
        }
        $rs = $getQuery->fetch();
       if($rs['identification'] != ""){
            echo "<br>".$rs['identification']." <font color='red'>duplicated</font>";
            echo "<script>console.log('".$rs['identification']." <font color=\'red\'>duplicated</font>')</script>";
            $d++;
       }else{
            echo "<br>".$value;
            echo "<script>console.log('".$value."')</script>";
       }
    }
    echo "<script>console.log('finish')</script>";
    echo "<br><br><br>Total :".$i;
    echo "<br>Duplicated :".$d;
    /*$rs_identification =  $assoc_identification['identification'];
    print_r($assoc_identification['identification']);

    foreach ($search as &$value) {
        if($value)
    }*/
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>

<form class="searchForm" name='searchForm' method='post' onSubmit='return validateForTemplate();' action='searchid.php'>

        <h2>Search duplicate id</h2>
            <table cellpadding="5" border="1" width="600">
            <tr><td>
            <h3>Event Operand</h3>
            <label><input type="radio" name="eOperand" value="" checked>None</label>
            <label><input type="radio" name="eOperand" value="only">Only</label>
            <label><input type="radio" name="eOperand" value="exclude">Exclude</label>
            </td>
            <td>
            <h3>Event Name</h3>
            <label><input type="checkbox" name="eventName" value="setinthecity-1">SET in the City 2017 ครั้งที่ 1</label><br/>
			<label><input type="checkbox" name="eventName" value="setinthecity-2">SET in the City 2017 ครั้งที่ 2</label><br/>
            <label><input type="checkbox" name="eventName" value="investnow-2">investnow กรุงเทพ ตลาดหลักทรัพย์ 2018 ครั้งที่ 2</label><br/>
            <label><input type="checkbox" name="eventName" value="nf-aws-saraburi-2018">New Frontier AWS สระบุรี 2018</label></td></tr>
            <tr><td>
            <h3>Broker Operand</h3>
            <label><input type="radio" name="bOperand" value="" checked>None</label>
            <label><input type="radio" name="bOperand" value="only">Only</label>
            <label><input type="radio" name="bOperand" value="exclude">Exclude</label>
            </td>
            <td>
            <h3>Broker Name</h3>
            <select name="brokerName">
            <option value="" selected>เลือกชื่อโบรกเกอร์</option>
            <?php

            $sql = "SELECT * FROM `sec_com`";
            try{
                $getQuery = $pdo->query($sql);
            }catch(PDOExeption $e){
                die("Query failed: ".$e.getMessage());
            }
            $results = $getQuery->fetchAll();
            foreach($results as $rs){
                echo "<option value='".$rs['name']."'>".$rs['name']."</option>";
            }

            ?>
            </select>
            </td></tr>
            </table>
            <br>
            <textarea name="searchArea" rows="10" cols="40"></textarea>
            <br>
        <input type="submit" value="Submit Query">
</form>
<br/>Search ID | <a href="convertbroker.php">Go Convert Broker Name</a><br>
</body>
<?php

$time_end = microtime(true);
    $time = $time_end - $time_start;
    echo "Process Time: {".$time."}";
    // Process Time: 1.0000340938568

?>